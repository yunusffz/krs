<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $judul; ?></title>
	<!--<link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet">-->
	<link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?=base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css" />
 
	<link rel="stylesheet" href="<?=base_url();?>assets/css/colorbox.css" />
 
	<link rel="stylesheet" href="<?=base_url();?>assets/css/ace.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-responsive.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-skins.min.css" />
</head>
<body class="login-layout">
<div class="main-container container-fluid">
<div class="main-content">
 
 <!--
	  
		<div id="bg-line">
		Username : 
		<?php echo form_input($username); ?>
		Password : 
		<?php echo form_input($password); ?>
		<?php echo form_submit('submit', 'Log In', ' class="btn-kirim-login"');?> 
		<?php echo form_reset('submit', 'Hapus',' class="btn-kirim-login"');?>
		</div>
		
	-->	
	<div class="row-fluid">
	<div class="span12">
	<div class="login-container">
		<div class="row-fluid">
			<div class="position-relative">
			<div class="center">
				<h3>
					<span class="white"><img src="<?=base_url()?>assets/stmik_bandung.png" alt="Site Logo" class="img-responsive" width=""></span>
					<br> 
					<br> 
					<span class="icon-hospital white">  Selamat Datang di</span>
					<br/>
					<span class="white">Sistem Informasi Akademik Online</span> 
				</h3> 
			</div>
			<h1>
			</h1>
				<div id="login-box" class="login-box visible widget-box no-border">
					<div class="widget-body">
						<div class="widget-main">
							<h4 class="header blue lighter bigger">
								<i class="icon-lock green"></i>
								Silahkan login <br/>untuk mengakses informasi akademik.
							</h4>

							<div class="space-6"></div>
							<?php if ($this->session->flashdata('message')){ ?>
								<div class="toolTip tpRed clearfix" style="color: #FFFFFF;" >
									<p class="red">
										<img src="<?=base_url()?>assets/img/exclamation-red.png" alt="Tip!" />
										<?php echo $this->session->flashdata('message'); ?>
									</p>
									<a class="close" title="Close"></a>
								</div>
							<?php } ?>
							<?php if ($this->session->flashdata('message2')){ ?>
								<div class="toolTip tpBlue clearfix" style="color: #FFFFFF;" >
									<p class="blue">
										<img src="<?=base_url()?>assets/img/exclamation-red.png" alt="Tip!" />
										<?php echo $this->session->flashdata('message2'); ?>
									</p>
									<a class="close" title="Close"></a>
								</div>
							<?php } ?>
							<?php echo form_open('web/login'); ?>
								<fieldset>
									<label>
										<span class="block input-icon input-icon-right">
											<input type="text" class="span12" id="username" name="username"  placeholder="Username" />
											<i class="icon-user"></i>
										</span>
									</label>

									<label>
										<span class="block input-icon input-icon-right">
											<input type="password" id="password" name="password" class="span12" placeholder="Password" />
											<i class="icon-lock"></i>
										</span>
									</label>

									<label class="inline">

										<span class="lbl"><a href="<?php echo base_url(); ?>web/forgotpassword"> Forgot Password ?</a></span>
									</label>
									
									
									<div class="clearfix">
										<label class="inline">
											<input type="checkbox" />
											<span class="lbl"> Remember Me</span>
										</label>

										<button type="submit"  class="width-35 pull-right btn btn-small btn-primary">
											<i class="icon-key"></i>
											Login
										</button>
									</div>

									<div class="space-4"></div>
								</fieldset>
							<?php echo form_close(); ?>
 
						</div><!--/widget-main-->

						<div class="toolbar clearfix">
							<div> 
							</div>

							<div>
								 
							</div>
						</div>
					</div><!--/widget-body-->
				</div><!--/login-box-->
 

			 
			</div><!--/position-relative-->
		</div>
		 
			<div>
				<div>
					<a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
						<i class="icon-arrow-left"></i>
						I forgot my password
					</a>
				</div>

				<div>
					<a href="#" onclick="show_box('signup-box'); return false;" class="user-signup-link">
						I want to register
						<i class="icon-arrow-right"></i>
					</a>
				</div>
			</div>
		</div><!--/widget-body-->
	</div><!--/widget-body--> 
	</div><!--/widget-body-->
	</div><!--/widget-body--> 
		  
<!--
<div>
	<p class="footer">
	Sistem Informasi Akademik (KRS)Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
	Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik
	</p>
</div> 
-->
</div>
</div>
</body>
</html>