	 

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="<?=site_url();?>admin/home">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							
						</li>
						<li class="active">	Artikel</li>
					</ul><!--.breadcrumb-->

					 
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Artikel Ubah Data
						</h1>
					</div><!--/.page-header-->

					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
							<div class="space-6"></div>
							<?if($alert){echo $alert;}?>
							
							<div class="row-fluid">
								  
								<?php
									$attributes = array('name' => 'artikel', 'type'=>'post', 'id' => 'artikel', 'class'=>'block-content form');
									echo form_open_multipart('admin/artikel/update', $attributes);
								?> 
								<?php $row = $results->row_array();?>	
								<input type="hidden" id="id_artikel"  name="id_artikel" placeholder="id_artikel" value="<?=$row['id_artikel']?>"> 
								
									<div class="form-horizontal control-group span6">
										<label class="control-label" for="form-field-1">Judul Artikel : </label>

										<div class="controls">
											<input type="text" id="judul_artikel"  name="judul_artikel" placeholder="judul_artikel" value="<?=$row['judul_artikel']?>">
										</div>
									</div> 
									
									<div class="form-horizontal control-group span6">
										<label class="control-label" for="form-field-1">Preview Artikel : </label>

										<div class="controls">
											<input type="text" name="preview_artikel" id="preview_artikel"  placeholder="preview artikel" value="<?=$row['preview_artikel']?>">
										</div>
									</div>
									
									<div class="form-horizontal control-group span6">
										<label class="control-label" for="form-field-1">Keterangan : </label>

										<div class="controls">
											<input type="text" name="keterangan" id="keterangan"  placeholder="keterangan" value="<?=$row['keterangan']?>">
										</div>
									</div>
									
									<div class="form-horizontal control-group span6">
										<label class="control-label" for="form-field-1">Gambar : </label>

										<div class="controls">  
											<input   name="userfile" id="userfile" type="file" value=""  size="42"/>
										</div>
									</div>
									   
									<div class="form-horizontal control-group span6"> 
										<label class="form-horizontal  control-label " for="form-field-1">Isi Artikel : </label>
										<div class="controls">
											<textarea id="isi_artikel" style="display:none" name="isi_artikel"><?=$row['isi_artikel'];?></textarea>
										</div>	
									</div>	 
									
									<div class="controls span11 ">
										<div class="space-6"></div>
										<div class="wysiwyg-editor"  name="editor1" id="editor1" ></div>
									</div>
									<div class="controls span6">
									<div class="space-6"></div>
										<button class="btn btn-primary" id="send"  name="send" type="submit" onclick="javascript:kirim();">
											<i class="icon-save bigger-125"></i>
											Simpan
										</button>
										<button class="btn btn-danger" type="button" onclick="javascript:batal();">
											<i class="icon-remove bigger-125"></i>
											Batal
										</button>
									</div>
											
								 
									
									<?php //} ?>
								</form> 
							</div> 

						</div> 
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

				 
			</div><!--/.main-content-->
			
<script type="text/javascript">

	window.onload=happycode; 

	function happycode()
	{  	
		
		var editor1 = document.getElementById("editor1");
		var editor2 = document.getElementById("isi_artikel");
		// alert(editor1.innerHTML);
		// alert(editor2.value);
		editor1.innerHTML  = editor2.value; 
	}
	
	function batal(){
		document.location.href = '<?=base_url().'admin/artikel'?>';
	}
	
	function kirim(){
		var editor1 = $('#editor1').html(); 
		 $('#isi_artikel').val(editor1);
		  
	} 

 
</script>
		 	
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>		
	
		<script src="<? echo site_url() ?>assets/admin/assets/js/jquery/2.0.3/jquery.min.js"></script>
  
		<script src="<?=site_url();?>assets/admin/assets/js/bootstrap.min.js"></script>

		<script src="<?=site_url();?>assets/admin/assets/js/ace-elements.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/ace.min.js"></script>
		 
		<script src="<?=site_url();?>assets/admin/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/markdown/markdown.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/markdown/bootstrap-markdown.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/jquery.hotkeys.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/bootstrap-wysiwyg.min.js"></script>
		<script src="<?=site_url();?>assets/admin/assets/js/bootbox.min.js"></script>
   


<script type="text/javascript">
$(function(){
	
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	}

	//$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

	//but we want to change a few buttons colors for the third style
	$('#editor1').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,
			{name:'createLink', className:'btn-pink'},
			{name:'unlink', className:'btn-pink'},
			null,
			{name:'insertImage', className:'btn-success'},
			null,
			'foreColor',
			null,
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		'wysiwyg': {
			fileUploadError: showErrorAlert
		}
	}).prev().addClass('wysiwyg-style2');

	

	$('#editor2').css({'height':'200px'}).ace_wysiwyg({
		toolbar_place: function(toolbar) {
			return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar);
		},
		toolbar:
		[
			'bold',
			{name:'italic' , title:'Change Title!', icon: 'icon-leaf'},
			'strikethrough',
			'underline',
			null,
			'insertunorderedlist',
			'insertorderedlist',
			null,
			'justifyleft',
			'justifycenter',
			'justifyright'
		],
		speech_button:false
	});


	$('[data-toggle="buttons-radio"]').on('click', function(e){
		var target = $(e.target);
		var which = parseInt($.trim(target.text()));
		var toolbar = $('#editor1').prev().get(0);
		if(which == 1 || which == 2 || which == 3) {
			toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
			if(which == 1) $(toolbar).addClass('wysiwyg-style1');
			else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
		}
	});





	//Add Image Resize Functionality to Chrome and Safari
	//webkit browsers don't have image resize functionality when content is editable
	//so let's add something using jQuery UI resizable
	//another option would be opening a dialog for user to enter dimensions.
	if ( typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase()) ) {
		
		var lastResizableImg = null;
		function destroyResizable() {
			if(lastResizableImg == null) return;
			lastResizableImg.resizable( "destroy" );
			lastResizableImg.removeData('resizable');
			lastResizableImg = null;
		}

		var enableImageResize = function() {
			$('.wysiwyg-editor')
			.on('mousedown', function(e) {
				var target = $(e.target);
				if( e.target instanceof HTMLImageElement ) {
					if( !target.data('resizable') ) {
						target.resizable({
							aspectRatio: e.target.width / e.target.height,
						});
						target.data('resizable', true);
						
						if( lastResizableImg != null ) {//disable previous resizable image
							lastResizableImg.resizable( "destroy" );
							lastResizableImg.removeData('resizable');
						}
						lastResizableImg = target;
					}
				}
			})
			.on('click', function(e) {
				if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
					destroyResizable();
				}
			})
			.on('keydown', function() {
				destroyResizable();
			});
	    }
		
		enableImageResize();

		/**
		//or we can load the jQuery UI dynamically only if needed
		if (typeof jQuery.ui !== 'undefined') enableImageResize();
		else {//load jQuery UI if not loaded
			$.getScript($<?=site_url();?>assets/admin/assets+"/js/jquery-ui-1.10.3.custom.min.js", function(data, textStatus, jqxhr) {
				if('ontouchend' in document) {//also load touch-punch for touch devices
					$.getScript($<?=site_url();?>assets/admin/assets+"/js/jquery.ui.touch-punch.min.js", function(data, textStatus, jqxhr) {
						enableImageResize();
					});
				} else	enableImageResize();
			});
		}
		*/
	}
	
	$('#userfile , #id-input-file-2').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:false,
		onchange:null,
		thumbnail:false //| true | large
		//whitelist:'gif|png|jpg|jpeg'
		//blacklist:'exe|php'
		//onchange:''
		//
	});
	

});
</script>