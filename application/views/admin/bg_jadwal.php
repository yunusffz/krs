			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Admin</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Jadwal Perkuliahan</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Manajemen Jadwal Kuliah - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<?php echo $this->session->flashdata('save_krs'); ?>
		
									<table id="sample-table-1" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>  
											<th class="center">
												<label>
													<input type="checkbox" />
													<span class="lbl"></span>
												</label>
											</th>  
											<th>Kode MK</th>
											<th>Mata Kuliah</th>
											<th>Smstr</th>	
											<th>SKS</th>
											<th>Dosen</th>
											<th>Dosen</th>
											<th>Kelas</th>
											<th>Jadwal</th>
											<th>Quota</th>
											<th>Peserta</th>
											<th>Calon Peserta</th>
											<th>
											<?php
											echo '<a href="'.base_url().'admin/tambah_jadwal"  class="btn btn-small btn-success cboxElement"  data-rel="colorbox"  ><i class="icon-plus"></i> Tambah Jadwal</a>';
											?>
											</th>
										</tr>
									</thead>

									<tbody>	
								
								
									 
										<?php Tampilkan_Mata_Kuliah($jadwal); ?>
										
										</tbody>	
									</table> 
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				 
  
	 
	
<?php
function Tampilkan_Mata_Kuliah($jdwl)
{
	$rows=array();
	$index=0;
	$temp='';
	$acuan=0;
	$rowspan=1;
	foreach ($jdwl->result_array() as $value) 
	{
		if(($temp=='') || ($value['kd_mk']!=$temp)) {			
			$rows[$index] = '<tr>
				<td class="center">
					<label>
						<input type="checkbox" />
						<span class="lbl"></span>
					</label>
				</td>
				<td>'.$value['kd_mk'].'</td>
				<td id="'.'nama_'.$value['kd_mk'].'">'.$value['nama_mk'].'</td>
				<td>'.$value['semester'].'</td>
				<td id="id'.$value['kd_mk'].'">'.$value['jum_sks'].'</td>';
				
				// $rowspan=1;				
				// $acuan=$index;
			}else if($value['kd_mk']==$temp) {
				// $rows[$index] = '<tr>';
				// $rowspan++;
				$rows[$index] = '<tr>
				<td class="center">
					<label>
						<input type="checkbox" />
						<span class="lbl"></span>
					</label>
				</td>
				<td>'.$value['kd_mk'].'</td>
				<td id="'.'nama_'.$value['kd_mk'].'">'.$value['nama_mk'].'</td>
				<td>'.$value['semester'].'</td>
				<td id="id'.$value['kd_mk'].'">'.$value['jum_sks'].'</td>';
				
				// $rowspan=1;				
				// $acuan=$index;
			}

			$rows[$acuan]=str_replace('rowspan="'.($rowspan-1).'"', 'rowspan="'.$rowspan.'"', $rows[$acuan]);
			$peserta = isset($value['Peserta']) ? $value['Peserta']:'0';
			$calonpeserta = isset($value['CalonPeserta']) ? $value['CalonPeserta']:'0';
		
			$disabled ='';
			if($peserta >= $value['kapasitas'])
				$disabled ='disabled="disabled"';
			
			$linkpeserta = $peserta;
			if($peserta >0)
			$linkpeserta = '<a href="'.base_url().'admin/peserta/'.$value['kd_jadwal'].'_1
			/" title="Daftar Peserta Mata Kuliah '.$value['nama_mk'].'  -  Dosen '.$value['nama_dosen'].'" rel="example_group" class="badge badge-info cboxElement"  data-rel="colorbox">'
				.$peserta.'</a>';
				
			$linkcalonpeserta = $calonpeserta;
			if($calonpeserta >0)
			$linkcalonpeserta = '<a href="'.base_url().'admin/peserta/'.$value['kd_jadwal'].'_0
			/" title="Daftar Calon Peserta Mata Kuliah '.$value['nama_mk'].'  -  Dosen '.$value['nama_dosen'].'" rel="example_group" class="badge badge-info cboxElement"  data-rel="colorbox">	
			'.$calonpeserta.'</a>';
						
			$rows[$index] .='<td id="'.'cell_'.$value['kd_mk'].'_'.$value['kelas'].'">'.$value['kd_dosen'].'</td><td>'.$value['nama_dosen'].'</td>
				<td>'.$value['kelas'].'</td>
				<td id="jdwl_'.$value['kd_jadwal'].'">'.$value['jadwal'].'</td>
				<td>'.$value['kapasitas'].'</td>
				<td>'.$linkpeserta.'</td>
				<td>'.$linkcalonpeserta.'</td>
				<td>
				<div class="center">
					<a class="purple" href="'.base_url().'admin/peserta_print/'.$value['kd_jadwal'].'_1'.'" title="Print"  data-rel="colorbox">
						<i class="icon-print bigger-130"></i>
					</a>&nbsp; &nbsp; 
					<a class="blue cboxElement" href="'.base_url().'admin/edit_jadwal/'.$value['kd_jadwal'].'" title="Edit"  data-rel="colorbox">
						<i class="icon-pencil bigger-130"></i>
					</a>&nbsp; &nbsp; 
					<a class="red" href="'.base_url().'admin/hapus_jadwal/'.$value['kd_jadwal'].'" title="Delete" onClick=\'return confirm("Anda yakin...??")\'>
						<i class="icon-trash bigger-130"></i>
					</a> 
				</div>
				</td></tr>';
			$index++;
			$temp=$value['kd_mk'];
	}		
	foreach($rows as $row)
	{
		echo str_replace('rowspan="1"', '', $row);
	}
}
?>

		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			minWidth:'60%',
			minHeight:'60%',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-1').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  { "bSortable": true },
		  null, null,null, null,  null, null,null,  null, null, null,
		  { "bSortable": false}
		] } );
		
		var oTable2 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,    null,   null, null, 
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	