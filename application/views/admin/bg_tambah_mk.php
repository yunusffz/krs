<!DOCTYPE html>
 
<?php echo $this->session->flashdata('save_mk'); ?>
<form method="post" action="<?php echo base_url(); ?>admin/simpan_mk">
<table>

<tr>
<td width="180">Kode Mata Kuliah</td>
<td width="50">:</td>
<td><input type="text" name="kd_mk" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Nama Mata Kuliah</td>
<td width="50">:</td>
<td><input type="text" name="nama_mk" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Jumlah SKS</td>
<td width="50">:</td>
<td><input type="text" name="jum_sks" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Semester</td>
<td width="50">:</td>
<td><input type="text" name="semester" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Prasyarat</td>
<td width="50">:</td>
<td><input type="text" name="prasyarat_mk" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Jurusan</td>
<td width="50">:</td>
<td><input type="text" name="kode_jur" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Data" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="stts" value="tambah"></td>
</tr>

</table>

</form>