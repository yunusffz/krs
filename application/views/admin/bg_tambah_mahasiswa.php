<!DOCTYPE html>
 
<?php echo $this->session->flashdata('save_mahasiswa'); ?>
<form method="post" action="<?php echo base_url(); ?>admin/simpan_mahasiswa">
<table>
<tr>
<td width="180">NIM</td>
<td width="50">:</td>
<td><input type="text" name="nim" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Nama Mahasiswa</td>
<td width="50">:</td>
<td><input type="text" name="nama_mahasiswa" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Angkatan</td>
<td width="50">:</td>
<td><input type="text" name="angkatan" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Jurusan</td>
<td width="50">:</td>
<td>
<select name="jurusan" class="input-read-only">
	<option value="IF">Teknik Informatika</option>
	<option value="SI">Sistem Informasi</option>
</select>
</td>
</tr>

<tr>
<td width="180">Kelas Program</td>
<td width="50">:</td>
<td>
<select name="kelas_program" class="input-read-only">
	<option value="REGULER">Reguler</option>
	<option value="KARYAWAN">Karyawan</option>
	<option value="EKSEKUTIF">Eksekutif</option>
</select>
</td>
</tr>

<tr>
<td width="180">Dosen Wali</td>
<td width="50">:</td>
<td>

<select name="kd_dosen" class="input-read-only">
<?php
	foreach($dosen->result_array() as $d)
	{
	?>
	<option value="<?php echo $d['kd_dosen']; ?>"><?php echo $d['kd_dosen'].' - '.$d['nama_dosen']; ?></option>
	<?php
	}
?>
</select>
</td>
</tr>
<tr>
<td width="180">Kelas</td>
<td width="50">:</td>
<td>
<select name="kelas" class="input-read-only">
	<?php
	foreach($kelas->result_array() as $d)
	{
	?>
	<option value="<?php echo $d['nama_kelas']; ?>"><?php echo $d['kelas'].' - '.$d['keterangan']; ?></option>
	<?php
	}
?>
</select>
</td>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Data" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="stts" value="tambah"></td>
</tr>
</table>

</form>