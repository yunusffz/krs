		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				 

				<ul class="nav nav-list">
					<li <? if($menu == 'beranda'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/">
							<i class="icon-home"></i>
							<span class="menu-text"> Beranda </span>
						</a>
					</li>
					
					<? 
						if (($menu['jadwal']=="active")or($menu['jadwalUTS']=="active")or($menu['jadwalUAS']=="active")){ 
							echo "<li class='active open'>"; 
						}else {
							echo "<li>";
						} ?>
					
							<a href="#" class="dropdown-toggle">
										<i class="icon-calendar"></i> 
										<span class="menu-text">
											Jadwal
										</span> 
										<b class="arrow icon-angle-down"></b>
									</a>
							<ul class="submenu"> 
									
									
										<?php 
											
												if ($menu['jadwal']){ 
													echo "<li class='".$menu['jadwalUTS']."'>"; 
												}else {
													echo "<li>";
												} 
													echo '<a href="'.base_url().'admin/'.'tampil_jadwal">'; 
													echo '<i class="icon-calendar"></i>';
													echo '<span class="menu-text"> Jadwal Perkuliahan </span>';
													echo '</a>'; 
												echo "</li>";
											
										?>
										<?php 
											
												if ($menu['jadwalUTS']){ 
													echo "<li class='".$menu['jadwalUTS']."'>"; 
												}else {
													echo "<li>";
												} 
													echo '<a href="'.base_url().'admin/'.'tampil_jadwalUTS">'; 
													echo '<i class="icon-calendar"></i>';
													echo '<span class="menu-text"> Jadwal UTS </span>';
													echo '</a>'; 
												echo "</li>";
											
										?> 
										<?php 
									
										if ($menu['jadwalUAS']){ 
											echo "<li class='".$menu['jadwalUAS']."'>"; 
										}else {
											echo "<li>";
										} 
											echo '<a href="'.base_url().'admin/'.'tampil_jadwalUAS">'; 
											echo '<i class="icon-calendar"></i>';
											echo '<span class="menu-text"> Jadwal UAS </span>';
											echo '</a>'; 
										echo "</li>";
									
								?>  
							</ul>
						</li>
					
					
				
					<li <? if($menu == 'dosen'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/dosen">
							<i class="icon-legal"></i>
							<span class="menu-text"> Dosen </span>
						</a>
					</li>
					<li <? if($menu == 'staff'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/staff">
							<i class="icon-user"></i>
							<span class="menu-text"> Staff </span>
						</a>
					</li>
					<li <? if($menu == 'mata_kuliah'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/mk">
							<i class="icon-book"></i>
							<span class="menu-text"> Mata Kuliah </span>
						</a>
					</li>
					<li <? if($menu == 'mahasiswa'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/mahasiswa">
							<i class="icon-group"></i>
							<span class="menu-text"> Mahasiswa </span>
						</a>
					</li>
					<li <? if($menu == 'info'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/info">
							<i class="icon-info"></i>
							<span class="menu-text"> Info Kampus </span>
						</a>
					</li>
					<li <? if($menu == 'tahun'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/tahun">
							<i class="icon-cog"></i>
							<span class="menu-text"> Pengaturan Tahun Akademik </span>
						</a>
					</li>
					<li <? if($menu == 'pengaturan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>admin/akun">
							<i class="icon-user"></i>
							<span class="menu-text"> Pengaturan Akun </span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>web/logout">
							<i class="icon-off"></i>
							<span class="menu-text"> Keluar </span>
						</a>
					</li>
 
 
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>

