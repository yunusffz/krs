<!DOCTYPE html>
 
<form method="post" action="<?php echo base_url(); ?>admin/simpan_mk">
<table>
<?php
	foreach($mk->result_array() as $m)
	{
?>
<tr>
<td width="180">Nama Mata Kuliah</td>
<td width="50">:</td>
<td><input type="text" name="nama_mk" size="50" class="input-read-only" value="<?php echo $m['nama_mk']; ?>" /></td>
</tr>

<tr>
<td width="180">Jumlah SKS</td>
<td width="50">:</td>
<td><input type="text" name="jum_sks" size="50" class="input-read-only" value="<?php echo $m['jum_sks']; ?>" /></td>
</tr>

<tr>
<td width="180">Semester</td>
<td width="50">:</td>
<td><input type="text" name="semester" size="50" class="input-read-only" value="<?php echo $m['semester']; ?>" /></td>
</tr>

<tr>
<td width="180">Prasyarat</td>
<td width="50">:</td>
<td><input type="text" name="prasyarat_mk" size="50" class="input-read-only" value="<?php echo $m['prasyarat_mk']; ?>" /></td>
</tr>

<tr>
<td width="180">Jurusan</td>
<td width="50">:</td>
<td><input type="text" name="kode_jur" size="50" class="input-read-only" value="<?php echo $m['kode_jur']; ?>" /></td>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Data" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="kd_mk" value="<?php echo $m['kd_mk']; ?>">
<input type="hidden" name="stts" value="edit"></td>
</tr>
<?php
	}
?>
</table>

</form>