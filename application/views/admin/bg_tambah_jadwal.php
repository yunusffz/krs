<!DOCTYPE html>
 
<form method="post" action="<?php echo base_url(); ?>admin/simpan_jadwal">
<table>

<tr>
<td width="180">Mata Kuliah</td>
<td width="50">:</td>
<td width="380">
<select name="kd_mk" class="input-read-only">
	<?php
		foreach($mata_kuliah->result_array() as $mk)
		{
			?>
			<option value="<?php echo $mk['kd_mk']; ?>"><?php echo $mk['kd_mk']." - ".$mk['nama_mk']; ?></option>
			<?php
		}
	?>
</select>
</td>
</tr>

<tr>
<td width="180">Nama Dosen</td>
<td width="50">:</td>
<td>
<select name="kd_dosen" class="input-read-only">
	<?php
		foreach($dosen->result_array() as $d)
		{
			?>
			<option value="<?php echo $d['kd_dosen']; ?>"><?php echo $d['kd_dosen']." - ".$d['nama_dosen']; ?></option>
			<?php
		}
	?>
</select>
</td>
</tr>

<tr>
<td width="180">Hari</td>
<td width="50">:</td>
<td>
<select name="hari" class="input-read-only">
	<option value="Senin">Senin</option>
	<option value="Selasa">Selasa</option>
	<option value="Rabu">Rabu</option>
	<option value="Kamis">Kamis</option>
	<option value="Jumat">Jumat</option>
	<option value="Sabtu">Sabtu</option>
	<option value="Minggu">Minggu</option>
	
</select>
</td>
</tr>

<tr>
<td width="180">Jam Mulai</td>
<td width="50">:</td>
<td>
<select name="jam_mulai" class="input-read-only">
	<?php
		foreach($jam->result_array() as $ja)
		{
			?>
			<option value="<?php echo $ja['Jam_mulai']; ?>"><?php echo $ja['sesi']." - ".$ja['Jam_mulai']; ?></option>
			<?php
		}
	?>
</select>
<!--<input type="text" name="jam_mulai" size="50" class="input-read-only" />--></td>
</tr>

<tr>
<td width="180">Jam Berakhir</td>
<td width="50">:</td>
<td><select name="jam_akhir" class="input-read-only">
	<?php
		foreach($jam->result_array() as $ja)
		{
			?>
			<option value="<?php echo $ja['Jam_akhir']; ?>"><?php echo $ja['sesi']." - ".$ja['Jam_akhir']; ?></option>
			<?php
		}
	?>
</select></td>
</tr>

<tr>
<td width="180">Ruangan</td>
<td width="50">:</td>
<td><input type="text" name="ruang" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Tahun Ajaran</td>
<td width="50">:</td>
<td>
<select name="kd_tahun" class="input-read-only">
	<?php
		foreach($tahun_ajaran->result_array() as $ta)
		{
			?>
			<option value="<?php echo $ta['kd_tahun']; ?>"><?php echo $ta['keterangan']; ?></option>
			<?php
		}
	?>
</select>
</td>
</tr>

<tr>
<td width="180">Kapasitas Kelas</td>
<td width="50">:</td>
<td><input type="text" name="kapasitas" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Kelas Program</td>
<td width="50">:</td>
<td>
<select name="kelas_program" class="input-read-only">
	<option value="Reguler">Reguler</option>
	<option value="Karyawan">Karyawan</option>
	<option value="Eksekutif">Eksekutif</option>
</select>
</td>
</tr>

<tr>
<td width="180">Kelas</td>
<td width="50">:</td>
<td>
<select name="kelas" class="input-read-only">
	<?php
	foreach($kelas->result_array() as $d)
	{
	?>
	<option value="<?php echo $d['nama_kelas']; ?>"><?php echo $d['kelas'].' - '.$d['keterangan']; ?></option>
	<?php
	}
?>
</select>
</td>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Nilai" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="stts" value="tambah"></td>
</tr>

</table>

</form>