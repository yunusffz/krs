<!DOCTYPE html>
 
<form method="post" action="<?php echo base_url(); ?>admin/simpan_mahasiswa">
<table>
<?php
	foreach($mahasiswa->result_array() as $m)
	{
?>
<tr>
<td width="180">Nama Mahasiswa</td>
<td width="50">:</td>
<td><input type="text" name="nama_mahasiswa" size="50" class="input-read-only" value="<?php echo $m['nama_mahasiswa']; ?>" /></td>
</tr>

<tr>
<td width="180">Angkatan</td>
<td width="50">:</td>
<td><input type="text" name="angkatan" size="50" class="input-read-only" value="<?php echo $m['angkatan']; ?>" /></td>
</tr>

<tr>
<td width="180">Jurusan</td>
<td width="50">:</td>
<td><select name="jurusan" class="input-read-only">
	<?php
		$IF = '';
		$SI = '';
		if($m['jurusan']=="IF")
		{
			$IF = 'selected="selected"';
			$SI = '';
		}
		else if($m['jurusan']=="SI")
		{
			$IF = '';
			$SI = 'selected="selected"';
		}
	?>
	<option value="IF" <?php echo $IF; ?>>Teknik Informatika</option>
	<option value="SI" <?php echo $SI; ?>>Sistem Informasi</option>
</select></td>
</tr>

<tr>
<td width="180">Kelas Program</td>
<td width="50">:</td>
<td>
<select name="kelas_program" class="input-read-only">
	<?php
		$reguler = '';
		$karyawan = '';
		$ekse = '';
		if($m['kelas_program']=="Reguler")
		{
			$reguler = 'selected="selected"';
			$karyawan = '';
			$ekse = '';
		}
		else if($m['kelas_program']=="KARYAWAN")
		{
			$reguler = '';
			$karyawan = 'selected="selected"';
			$ekse = '';
		}
		else if($m['kelas_program']=="EKSEKUTIF")
		{
			$reguler = '';
			$karyawan = '';
			$ekse = 'selected="selected"';
		}
	?>
	<option value="Reguler" <?php echo $reguler; ?>>Reguler</option>
	<option value="Karyawan" <?php echo $karyawan; ?>>Karyawan</option>
	<option value="Eksekutif" <?php echo $ekse; ?>>Eksekutif</option>
</select>
</td>
</tr>

<tr>
<td width="180">Dosen Wali</td>
<td width="50">:</td>
<td>

<select name="kd_dosen" class="input-read-only">
<?php
	foreach($dosen->result_array() as $d)
	{
	$selected = '';
	if($d['kd_dosen']==$m['kd_dosen'])
	{
		$selected = 'selected="selected"';
	}
	?>
	<option value="<?php echo $d['kd_dosen']; ?>" <?php echo $selected; ?>><?php echo $d['kd_dosen'].' - '.$d['nama_dosen']; ?></option>
	<?php
	}
?>
</select>
</td>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Data" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="nim" value="<?php echo $m['nim']; ?>">
<input type="hidden" name="stts" value="edit"></td>
</tr>
<?php
	}
?>
</table>

</form>