<!DOCTYPE html>
 
<?php echo $this->session->flashdata('save_staff'); ?>
<form method="post" action="<?php echo base_url(); ?>admin/simpan_staff">
<table>

<tr>
<td width="180">Kode Staff</td>
<td width="50">:</td>
<td><input type="text" name="kd_staff" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">NIP</td>
<td width="50">:</td>
<td><input type="text" name="nip" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">Nama Staff</td>
<td width="50">:</td>
<td><input type="text" name="nama_staff" size="50" class="input-read-only" /></td>
</tr>

<tr>
<td width="180">KBB </td>
<td width="50">:</td>
<td>
<select name="kbb" class="input-read-only">
	<option value="Pusat">Pusat</option>
	<option value="Tasik 1">Tasik 1</option>
	<option value="Tasik 2">Tasik 2</option>
	<option value="Cianjur 1">Cianjur 1</option>
	<option value="Cianjur 2">Cianjur 2</option>
	<option value="Ciamis">Ciamis</option>
	<option value="BandungBarat">Cimahi - Bandung Barat</option>
</select>
</tr>

<tr>
<td width="180"></td>
<td width="50"></td>
<td>
<input type="submit" value="Simpan Data" class="btn btn-info">
<input type="reset" value="Batal" class="btn btn-danger">
<input type="hidden" name="stts" value="tambah"></td>
</tr>

</table>

</form>