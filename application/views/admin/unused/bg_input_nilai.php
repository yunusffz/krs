
<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css" />


<link rel="stylesheet" href="<?=base_url()?>assets/css/colorbox.css" />



<link rel="stylesheet" href="<?=base_url()?>assets/css/ace.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-responsive.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-skins.min.css" />
 
<div id="container">
	<h1>Nilai - Kartu Hasil Studi - Sistem Informasi Akademik Online</h1>

	<div id="body">
		<?php
			echo $bio;
			echo $menu;
		?>
		<div class="cleaner_h10"></div>
		
		<table id="sample-table-1" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>  
				<th colspan="12" class="center">Mata Kuliah Yang Akan Diinputkan Nilainya :</th> 
			</tr>
			<tr>  
				<th align="center">Kode MK</th>
				<th align="center">Mata Kuliah</th>
				<th align="center">Smstr</th>	
				<th align="center">SKS</th>
				<th align="center">Dosen</th>
				<th align="center">Kelas</th>
				<th align="center">Jadwal</th>
				<th align="center">Quota</th>
				<th align="center">Peserta</th>
				<th align="center">*</th>
				<?php
				if($status=='0')
				{
					echo '<th align="center">Batalkan</th>';
				}
				?>
			</tr>
		</thead>

		<tbody>	
		
			


<?php
	$no=1;
	$tot_sks = 0;
	foreach ($detailfrs->result_array() as $value) 
	{
	$tot_sks += $value['jum_sks'];
		
		echo '<tr class="content">
				<td>'.$value['kd_mk'].'</td>
				<td>'.$value['nama_mk'].'</td>
				<td>'.$value['semester'].'</td>
				<td>'.$value['jum_sks'].'</td>';
				
		echo '<td>'.$value['nama_dosen'].'</td>
				<td align="center">'.$value['kelas'].'</td>
				<td align="center">'.$value['jadwal'].'</td>
				<td align="center">'.$value['kapasitas'].'</td>
				<td align="center">'.$value['Peserta'].'</td>
				<td align="center"><a href="'.base_url().'admin/form_input_nilai/'.$value['nim'].'/'.$value['kd_jadwal'].'" class="link cboxElement"  data-rel="colorbox"
				rel="example_group">Input</a></td>';
			if($status=='0')
			{
				echo '<td align="center">
				<a class="delbutton" id="'.$value['nim'].'|'.$value['kd_jadwal'].'" href="#"><div id="box-link">Batalkan</div></a>
				</td>';
			}
	}
	echo '<tr><td colspan=3>Total SKS Yang Akan Ditempuh :</td><td colspan=8 id="jmlcart"><b>'.$tot_sks.' SKS</b></td></tr>';
?>
		<tbody>	
		</table>
		
		
		<div class="cleaner_h40"></div>
		<?php 
		$temp='';
		$rows=array();
		$totalNH=0;	
		$totalSKS=0;
		$no=1;
		?>
		<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>  
				<th colspan="12" class="center">Mata Kuliah yang Tersimpan :</th> 
			</tr>
			<tr>  
				<th align="center">No</th>
				<th align="center">Kode Mata Kuliah</th>
				<th align="center">Mata Kuliah</th>
				<th align="center">Semester</th>
				<th align="center">SKS</th>
				<th align="center">Nilai</th>	
				<th align="center">Bobot</th>
				<th align="center">SKS x Bobot</th>
				<th colspan="2">Aksi</th>
			</tr>
		</thead>

		<tbody>	
		
		
		<?php
		foreach($khs->result_array() as $value)
		{
			if($temp=='')
			{
				$rows[]='<tr>
				<td colspan="10" bgcolor="#fff"><strong>Semester : '.$value['semester_ditempuh'].'</strong></td>
				</tr>';
				$rows[]='<tr>
				<td>'. $no.'</td>
				<td>'. $value['kd_mk'].'</td>
				<td>&nbsp;'. $value['nama_mk'].'</td>
				<td align="center">'. $value['semester_ditempuh'].'</td>
				<td align="center">'. $value['jum_sks'].'&nbsp;</td>
				<td align="center">'. $value['grade'].'</td>
				<td align="center">'. $value['bobot'].'</td>
				<td align="center">'. $value['NxH'].'</td>
				<td align="center"><a href="'.base_url().'admin/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link cboxElement"  data-rel="colorbox"
				rel="example_group">Edit</a></td>
				<td align="center"><a href="'.base_url().'admin/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link"
				onClick=\'return confirm("Anda yakin...??")\'>Hapus</a></td>';
				$no++;
				$totalNH=0;
				$totalSKS=0;
			}
			else if($value['semester_ditempuh']!=$temp)
			{
				$ip = 0;
				if($totalNH !=0)			
					$ip = round($totalNH/$totalSKS, 2);			
				$rows[]='<tr>
				<td colspan="6"><strong>Jumlah SKS : '.$totalSKS.'</strong></td>
				<td colspan="6"><strong>IP Semester : '.$ip.'</strong></td>';
	
				$rows[]='<tr>
				<td colspan="10" bgcolor="#fff"><strong>Semester : '.$value['semester_ditempuh'].'</strong></td>
				</tr>';
	
				$rows[]='<tr>
				<td>'. $no.'</td>
				<td>'. $value['kd_mk'].'</td>
				<td>&nbsp;'. $value['nama_mk'].'</td>
				<td align="center">'. $value['semester_ditempuh'].'</td>
				<td align="center">'. $value['jum_sks'].'&nbsp;</td>
				<td align="center">'. $value['grade'].'</td>
				<td align="center">'. $value['bobot'].'</td>
				<td align="center">'. $value['NxH'].'</td>
				<td align="center"><a href="'.base_url().'admin/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link cboxElement"  data-rel="colorbox"
				rel="example_group">Edit</a></td>
				<td align="center"><a href="'.base_url().'admin/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link"
				onClick=\'return confirm("Anda yakin...??")\'>Hapus</a></td>
			</tr>';
			$no++;
			
				$totalNH =0;
				$totalSKS=0;
			}		
			else 
			{ 
				$rows[]='<tr>
				<td>'. $no.'</td>
				<td>'. $value['kd_mk'].'</td>
				<td>&nbsp;'. $value['nama_mk'].'</td>
				<td align="center">'. $value['semester_ditempuh'].'</td>
				<td align="center">'. $value['jum_sks'].'</td>
				<td align="center">'. $value['grade'].'</td>
				<td align="center">'. $value['bobot'].'</td>
				<td align="center">'. $value['NxH'].'</td>
				<td align="center"><a href="'.base_url().'admin/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link cboxElement"  data-rel="colorbox"
				rel="example_group">Edit</a></td>
				<td align="center"><a href="'.base_url().'admin/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="link"
				onClick=\'return confirm("Anda yakin...??")\'>Hapus</a></td>
			</tr>';
			$no++;
						
			}
			if($value['grade'] != 'T') {
				$totalNH +=$value['NxH'];
				$totalSKS+=$value['jum_sks'];
			}
			$temp=$value['semester_ditempuh'];	
		}
		$ip = 0;
		if($totalNH !=0)			
			$ip = round($totalNH/$totalSKS, 2);
		$rows[]='
				<tr>
				<td colspan="6"><strong>Jumlah SKS : '.$totalSKS.'</strong></td>
				<td colspan="6"><strong>IP Semester : '.$ip.'</strong></td>
				</tr>';
	
		foreach($rows as $row)
		{
			echo $row;
		}
		?>
		
		<tbody>	
		</table>
	 
		
	</div>

		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	  	
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	