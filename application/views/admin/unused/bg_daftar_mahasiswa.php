
<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css" />


<link rel="stylesheet" href="<?=base_url()?>assets/css/colorbox.css" />



<link rel="stylesheet" href="<?=base_url()?>assets/css/ace.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-responsive.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-skins.min.css" />
 
<div id="container">
	<h1>Input Nilai - Sistem Informasi Akademik Online</h1>

	<div id="body">
		<?php
			echo $bio;
			echo $menu;
		?>
		<div class="cleaner_h10"></div>
		
		<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead> 
			<tr>  
				<th>No.</th>
				<th>Nama Mahasiswa</th>
				<th>Jurusan</th>
				<th>Program Kelas</th>
				<th>Status Persetujuan</th>
				<th>Masukkan Nilai</th> 
			</tr>
		</thead>

		<tbody>	
		 
		<?php
			$no=1;
			foreach($mhs->result_array() as $k)
			{
					$st = "";
					if($k['status']=='1'){ 
						$st = "Sudah Disetujui"; 
						$warna = "#ccc";
						$link = base_url().'edit/detail_krs/'.$k['nim'].'/'.$k['status'];
						$cf = "example_group";
					} 
					echo'<tr>
					<td align="center">'.$no.'</td><td>'.$k['nim'].'</td><td>'.$k['nama_mahasiswa'].'</td><td>'.$k['jurusan'].'</td><td align="center">
					'.$k['kelas_program'].'</td><td align="center">'.$st.'</td>';
		
					echo'<td>
					<a class="link cboxElement"  data-rel="colorbox" href="'.base_url().'admin/input_nilai/'.$k['nim'].'" title="Masukkan Nilai - '.$k[
					'nama_mahasiswa'].'">Masukkan Nilai</a></td>
					</tr>';
					$no++;
			}
		?>
		</table>
		 
	</div>

		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,null, null, 
		  { "bSortable": false }
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	