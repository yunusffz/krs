			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Admin</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Dosen</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Daftar Dosen - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<?php echo $this->session->flashdata('save_dosen'); ?>
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
										<thead> 
											<tr>  
												<th class="center">
													<label>
														<input type="checkbox" />
														<span class="lbl"></span>
													</label>
												</th>  
												<th>Kode Dosen.</th>
												<th>NIDN</th>
												<th>Nama Dosen</th>
												<th class="center"><?php
													echo '<a href="'.base_url().'admin/tambah_dosen" rel="example_group" class="btn btn-small btn-success cboxElement"  data-rel="colorbox"  ><i class="icon-plus"></i> Tambah Dosen</a>';
													?>
												</th> 
											</tr>
										</thead>

										<tbody>	
									
									
									<?php
										foreach($dosen->result_array() as $d)
										{
										?>
											<tr>
											<td class="center">
												<label>
													<input type="checkbox" />
													<span class="lbl"></span>
												</label>
											</td>
											<td><?php echo $d['kd_dosen']; ?></td>
											<td><?php echo $d['nidn']; ?></td>
											<td><?php echo $d['nama_dosen']; ?></td>	
											<?php 
											echo '<td class="center">
											<a class="badge badge-info " href="'.base_url().'admin/dosen_mk/'.$d['kd_dosen'].'"  tyle="float:left;">MK</a> 
											 
											&nbsp; &nbsp; 
											<a class="blue cboxElement" href="'.base_url().'admin/edit_dosen/'.$d['kd_dosen'].'" title="Edit"  data-rel="colorbox">
												<i class="icon-pencil bigger-130"></i>
											</a>&nbsp; &nbsp; 
											<a class="red" href="'.base_url().'admin/hapus_dosen/'.$d['kd_dosen'].'" title="Delete" onClick=\'return confirm("Anda yakin...??")\'>
												<i class="icon-trash bigger-130"></i>
											</a> 
											
											</td>';
											?>
											</tr>
										<?php
										}
									?>
										</tbody>	
									</table>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				
		 

		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  { "bSortable": true },
		  null, null,  
		  { "bSortable": false }
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	