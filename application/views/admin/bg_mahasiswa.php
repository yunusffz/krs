			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Admin</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Mahasiswa</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Daftar Mahasiswa - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<?php echo $this->session->flashdata('save_mahasiswa'); ?>
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>   
										<tr>  
											<th class="center">
												<label>
													<input type="checkbox" />
													<span class="lbl"></span>
												</label>
											</th>  
											<th>NIM</th>
											<th>Nama Mahasiswa</th>
											<th>Angkatan</th>	
											<th>Jurusan</th>	
											<th>Kelas Program</th> 
											<th class="center">
											<?php
												echo '<a href="'.base_url().'admin/tambah_mahasiswa" rel="example_group" class="btn btn-small btn-success cboxElement"  data-rel="colorbox"  ><i class="icon-plus"></i> Tambah Mahasiswa</a>';
											?>
											</th>
										</tr>
									</thead>

									<tbody>	
								
								 
								
								<?php
									foreach($mahasiswa->result_array() as $d)
									{
									?>
										<tr>
										<td class="center" rowspan="1">
											<label>
												<input type="checkbox" />
												<span class="lbl"></span>
											</label>
										</td>
										<td><?php echo $d['nim']; ?></td>	
										<td><?php echo $d['nama_mahasiswa']; ?></td>
										<td><?php echo $d['angkatan']; ?></td>
										<td><?php echo $d['jurusan']; ?></td>
										<td><?php echo $d['kelas_program']; ?></td>	
										<td class="center">
										<?php
										echo '
										 
											<a class="blue cboxElement" href="'.base_url().'admin/edit_mahasiswa/'.$d['nim'].'" title="Edit"  data-rel="colorbox">
												<i class="icon-pencil bigger-130"></i>
											</a>&nbsp; &nbsp; 
											<a class="red" href="'.base_url().'admin/hapus_mahasiswa/'.$d['nim'].'" title="Delete" onClick=\'return confirm("Anda yakin...??")\'>
												<i class="icon-trash bigger-130"></i>
											</a> 
											';
											
											
										?>
										</td>	
										</tr>
									<?php
									}
								?>
								
									</tbody>	
								</table> 
										
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				 
		
  
		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-2').dataTable( {
		"aoColumns": [ 
		 
		  {"bSortable": false }, 
		  null,
		  null,
		  null,
		  null,   
		  null,   
		  { "bSortable": false  }
		] } );
		
		 
		
		 
	})
	 
	</script>	