			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Admin</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Pengaturan Akun</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Manajemen Akun - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<?php echo $this->session->flashdata('save_akun'); ?>
		
									<form method="post" action="<?php echo base_url(); ?>admin/simpan_akun">
									<table border="0"   cellpadding="5">

									<tr>
										<td>Password Lama</td>
										<td>:</td>
										<td><input type="password" name="pass_lama" required/></td>
									</tr>
									<tr>
										<td>Password Baru</td>
										<td>:</td>
										<td><input type="password" name="pass_baru" required/></td>
									</tr>
									<tr>
										<td>Ulangi Password Baru</td>
										<td>:</td>
										<td><input type="password" name="ulangi_pass" required/></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td><input type="submit" value="Simpan Data" class="btn btn-primary"/></td>
									</tr>
										
									
									</table>
									</form>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
 