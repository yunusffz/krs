
	<script src="<?php echo base_url('js/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.custom.min.js') ?>"></script>
	<script src="<?php echo base_url('js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('js/jquery.jeditable.mini.js') ?>"></script>
	<script src="<?php echo base_url('js/main.js') ?>"></script>
<link href="<?php echo base_url(); ?>datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>asset/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>asset/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>asset/js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="<?php echo base_url(); ?>datatables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$("a[rel=example_group]").fancybox({
				'height'			: '95%',
				'width'				: '70%',
				'transitionIn'		: 'fade',
				'transitionOut'		: 'fade',
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.9,
				'type'              : 'iframe',
				'showNavArrows'   : false,
				'hideOnOverlayClick': false,
				'onClosed'          : function() {
									  parent.location.reload(true);
									  }
			});});
</script>
<script charset="utf-8" type="text/javascript">
   $(document).ready(function() {
    $('#mhs').dataTable({
     "sPaginationType": "full_numbers"
    });
   } );
   
</script>
<div id="container">
	<h1>Persetujuan KRS - Sistem Informasi Akademik Online</h1>

	<div id="dt_example">
		<?php
			echo $bio;
			echo $menu;
		?>
		<div class="cleaner_h10"></div>
		<table class="display" id="mhs" border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse;">
		<thead>
	<tr>
		<th>No</th>
		<th>Nim</th>
		<th>Nama Mahasiswa</th>
		<th>Jurusan</th>
		<th>Program</th>
		<th>Status</th>
		<th>Kelas</th>
		<th>Action</th>
	</tr>
		</thead>
		<?php
		$no=1;
			foreach($mhs->result_array() as $d)
		{
		?>
			<tr>
			<td class="center"><?php echo $no;?></td>
			<td class="center"><?php echo $d['nim']; ?></td>
			<td class="center"><?php echo $d['nama_mahasiswa']; ?></td>	
			<td class="center"><?php echo $d['jurusan']; ?></td>
			<td class="center"><?php echo $d['kelas_program']; ?></td>				
			<td class="center"><?php echo $bc['kode']=$this->web_app_model->getAktifasi($d['nim']); ?></td>
			<td class="center"><?php echo $d['kbb']; ?></td>	
			<?php 
			echo '<td class="center" width="10"><a href="'.base_url().'keuangan/reg_aktivasi/'.$d['nim'].'" rel="example_group" class="link" style="float:left;">AKTIVASI</a></td>';
			?>
			</tr>
			<?php $no++; ?>
			</tr>
		<?php
		}

		?>
		</table>
		
	</div>
