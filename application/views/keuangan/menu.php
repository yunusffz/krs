 
 		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				 

				<ul class="nav nav-list">
					<li <? if($menu == 'beranda'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>keuangan/">
							<i class="icon-home"></i>
							<span class="menu-text"> Beranda </span>
						</a>
					</li>
					
					<? 
						if (($menu['persetujuan']=="active")or($menu['persetujanUTS']=="active")or($menu['persetujanUAS']=="active")or($menu['lulus']=="active")){ 
							echo "<li class='active open'>"; 
						}else {
							echo "<li>";
						} ?>
					
							<a href="#" class="dropdown-toggle">
										<i class="icon-check"></i> 
										<span class="menu-text">
											REGISTRASI
										</span> 
										<b class="arrow icon-angle-down"></b>
									</a>
							<ul class="submenu"> 
									
										<?php 
											
												if ($menu['Persetujuan']){ 
													echo "<li class='".$menu['Persetujuan']."'>"; 
												}else {
													echo "<li>";
												} 
													echo '<a href="'.base_url().'keuangan/'.'Persetujuan">'; 
													echo '<i class="icon-check"></i>';
													echo '<span class="menu-text"> REGISTRASI / KRS </span>';
													echo '</a>'; 
												echo "</li>";
											
										?> 
										<?php 
											
												if ($menu['PersetujuanUTS']){ 
													echo "<li class='".$menu['PersetujuanUTS']."'>"; 
												}else {
													echo "<li>";
												} 
													echo '<a href="'.base_url().'keuangan/'.'PersetujuanUTS">'; 
													echo '<i class="icon-check"></i>';
													echo '<span class="menu-text"> AKTIVASI UTS </span>';
													echo '</a>'; 
												echo "</li>";
											
										?> 
										<?php 
									
										if ($menu['persetujuanUAS']){ 
											echo "<li class='".$menu['persetujuanUAS']."'>"; 
										}else {
											echo "<li>";
										} 
											echo '<a href="'.base_url().'keuangan/'.'persetujuanUAS">'; 
											echo '<i class="icon-check"></i>';
											echo '<span class="menu-text"> AKTIVASI UAS </span>';
											echo '</a>'; 
										echo "</li>";
									
								?> 
								<?php 
									
										if ($menu['lulus']){ 
											echo "<li class='".$menu['lulus']."'>"; 
										}else {
											echo "<li>";
										} 
											echo '<a href="'.base_url().'keuangan/'.'lulus">'; 
											echo '<i class="icon-check"></i>';
											echo '<span class="menu-text"> WISUDAWAN </span>';
											echo '</a>'; 
										echo "</li>";
									
								?>  								
							</ul>
						</li>
					
					<li <? if($menu == 'info'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>keuangan/info">
							<i class="icon-pencil"></i>
							<span class="menu-text"> Info Kampus </span>
						</a>
					</li> 
					<li <? if($menu == 'laporan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>keuangan/laporan">
							<i class="icon-book"></i>
							<span class="menu-text"> Laporan * </span>
						</a>
					</li>
					<li <? if($menu == 'pengaturan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>keuangan/akun">
							<i class="icon-cog"></i>
							<span class="menu-text"> Pengaturan Akun </span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>web/logout">
							<i class="icon-off"></i>
							<span class="menu-text"> Keluar </span>
						</a>
					</li>
 
 
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
 