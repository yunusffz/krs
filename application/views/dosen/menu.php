		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				 

				<ul class="nav nav-list">

					<li <? if($menu == 'beranda'){echo "class='active'";}?>>
						<a href="<?php echo base_url();?>dosen/">
							<i class="icon-home"></i>
							<span class="menu-text"> Beranda </span>
						</a>
					</li>
					<li <? if($menu == 'persetujuan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>dosen/persetujuan">
							<i class="icon-check"></i>
							<span class="menu-text"> Persetujuan KRS </span>
						</a>
					</li> 
					<li <? if($menu == 'perubahan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>dosen/prs">
							<i class="icon-exchange"></i>
							<span class="menu-text"> Perubahan KRS </span>
						</a>
					</li>
					<li <? if($menu == 'pengaturan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>dosen/akun">
							<i class="icon-cog"></i>
							<span class="menu-text"> Pengaturan Akun </span>
						</a>
					</li>
					<li <? if($menu == 'matakuliah'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>dosen/matakuliah">
							<i class="icon-book"></i>
							<span class="menu-text"> Mata Kuliah </span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>web/logout">
							<i class="icon-off"></i>
							<span class="menu-text"> Keluar </span>
						</a>
					</li>
 
 
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>

 