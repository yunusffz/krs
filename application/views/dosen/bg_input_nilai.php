			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Dosen</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Input Nilai</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Nilai - Kartu Hasil Studi - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<table id="sample-table-1" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>  
											<th colspan="12" class="center">Mata Kuliah Yang Akan Diinputkan Nilainya :</th> 
										</tr>
										<tr>  
											<th class="center">
												<label>
													<input type="checkbox" />
													<span class="lbl"></span>
												</label>
											</th>
											<th class="center">Kode MK</th>
											<th class="center">Mata Kuliah</th>
											<th class="center">Smstr</th>	
											<th class="center">SKS</th>
											<th class="center">Dosen</th>
											<th class="center">Kelas</th>
											<th class="center">Jadwal</th>
											<th class="center">Quota</th>
											<th class="center">Peserta</th>
											<th class="center">*</th>
											<?php
											if($status=='0')
											{
												echo '<th class="center">Batalkan</th>';
											}
											?>
										</tr>
									</thead>

									<tbody>	
									
										


							<?php
								$no=1;
								$tot_sks = 0;
								foreach ($detailfrs->result_array() as $value) 
								{
								$tot_sks += $value['jum_sks'];
									
									echo '<tr class="content">
												<td class="center">
													<label>
														<input type="checkbox" />
														<span class="lbl"></span>
													</label>
												</td>
											<td>'.$value['kd_mk'].'</td>
											<td>'.$value['nama_mk'].'</td>
											<td>'.$value['semester'].'</td>
											<td>'.$value['jum_sks'].'</td>';
											
									echo '<td>'.$value['nama_dosen'].'</td>
											<td class="center">'.$value['kelas'].'</td>
											<td class="center">'.$value['jadwal'].'</td>
											<td class="center">'.$value['kapasitas'].'</td>
											<td class="center">'.$value['Peserta'].'</td>
											<td class="center"><a href="'.base_url().'dosen/form_input_nilai/'.$value['nim'].'/'.$value['kd_jadwal'].'" class="btn btn-small btn-success cboxElement"  data-rel="colorbox"
											rel="example_group"><i class="icon-plus"></i> Input</a></td>';
										if($status=='0')
										{
											echo '<td class="center">
											<a class="delbutton" id="'.$value['nim'].'|'.$value['kd_jadwal'].'" href="#"><div id="box-link">Batalkan</div></a>
											</td>';
										}
								}
								echo '<tr><td colspan=3>Total SKS Yang Akan Ditempuh :</td><td colspan=8 id="jmlcart"><b>'.$tot_sks.' SKS</b></td></tr>';
							?>
									<tbody>	
									</table>
									
									
									<div class="cleaner_h40"></div>
									<?php 
									$temp='';
									$rows=array();
									$totalNH=0;	
									$totalSKS=0;
									$no=1;
									?>
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>  
											<th colspan="12" class="center">Mata Kuliah yang Tersimpan :</th> 
										</tr>
										<tr>  
											<th class="center">
												<label>
													<input type="checkbox" />
													<span class="lbl"></span>
												</label>
											</th>
											<th class="center">No</th>
											<th class="center">Kode Mata Kuliah</th>
											<th class="center">Mata Kuliah</th>
											<th class="center">Semester</th>
											<th class="center">SKS</th>
											<th class="center">Nilai</th>	
											<th class="center">Bobot</th>
											<th class="center">SKS x Bobot</th>
											<th colspan="2">Aksi</th>
										</tr>
									</thead>

									<tbody>	
									
									
									<?php
									foreach($khs->result_array() as $value)
									{
										if($temp=='')
										{
											$rows[]='<tr>
											<td colspan="11" bgcolor="#fff"><strong>Semester : '.$value['semester_ditempuh'].'</strong></td>
											</tr>';
											$rows[]='<tr>
												<td class="center">
													<label>
														<input type="checkbox" />
														<span class="lbl"></span>
													</label>
												</td>
											<td>'. $no.'</td>
											<td>'. $value['kd_mk'].'</td>
											<td>&nbsp;'. $value['nama_mk'].'</td>
											<td class="center">'. $value['semester_ditempuh'].'</td>
											<td class="center">'. $value['jum_sks'].'&nbsp;</td>
											<td class="center">'. $value['grade'].'</td>
											<td class="center">'. $value['bobot'].'</td>
											<td class="center">'. $value['NxH'].'</td>
											<td class="center">
											<a href="'.base_url().'dosen/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="blue cboxElement"  data-rel="colorbox"
											rel="example_group"><i class="icon-pencil bigger-130"></i></a>
											 
											</td>
											<td class="center">
											<a href="'.base_url().'dosen/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="red"
											onClick=\'return confirm("Anda yakin...??")\'><i class="icon-trash bigger-130"></i></a>
											</td>';
											$no++;
											$totalNH=0;
											$totalSKS=0;
										}
										else if($value['semester_ditempuh']!=$temp)
										{
											$ip = 0;
											if($totalNH !=0)			
												$ip = round($totalNH/$totalSKS, 2);			
											$rows[]='<tr>
											<td colspan="6"><strong>Jumlah SKS : '.$totalSKS.'</strong></td>
											<td colspan="6"><strong>IP Semester : '.$ip.'</strong></td>';
								
											$rows[]='<tr>
											<td colspan="11" bgcolor="#fff"><strong>Semester : '.$value['semester_ditempuh'].'</strong></td>
											</tr>';
								
											$rows[]='<tr>
												<td class="center">
													<label>
														<input type="checkbox" />
														<span class="lbl"></span>
													</label>
												</td>
											<td>'. $no.'</td>
											<td>'. $value['kd_mk'].'</td>
											<td>&nbsp;'. $value['nama_mk'].'</td>
											<td class="center">'. $value['semester_ditempuh'].'</td>
											<td class="center">'. $value['jum_sks'].'&nbsp;</td>
											<td class="center">'. $value['grade'].'</td>
											<td class="center">'. $value['bobot'].'</td>
											<td class="center">'. $value['NxH'].'</td>
											<td class="center"><a href="'.base_url().'dosen/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="blue cboxElement"  data-rel="colorbox"
											rel="example_group"><i class="icon-pencil bigger-130"></a></td>
											<td class="center"><a href="'.base_url().'dosen/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="red"
											onClick=\'return confirm("Anda yakin...??")\'><i class="icon-trash bigger-130"></i></a></td>
										</tr>';
										$no++;
										
											$totalNH =0;
											$totalSKS=0;
										}		
										else 
										{ 
											$rows[]='<tr>
												<td class="center">
													<label>
														<input type="checkbox" />
														<span class="lbl"></span>
													</label>
												</td>
											<td>'. $no.'</td>
											<td>'. $value['kd_mk'].'</td>
											<td>&nbsp;'. $value['nama_mk'].'</td>
											<td class="center">'. $value['semester_ditempuh'].'</td>
											<td class="center">'. $value['jum_sks'].'</td>
											<td class="center">'. $value['grade'].'</td>
											<td class="center">'. $value['bobot'].'</td>
											<td class="center">'. $value['NxH'].'</td>
											<td class="center"><a href="'.base_url().'dosen/edit_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="blue cboxElement"  data-rel="colorbox"
											rel="example_group"><i class="icon-pencil bigger-130"></i></a></td>
											<td class="center"><a href="'.base_url().'dosen/hapus_nilai/'.$value['nim'].'/'.$value['kd_mk'].'" class="red"
											onClick=\'return confirm("Anda yakin...??")\'><i class="icon-trash bigger-130"><>/i></a></td>
										</tr>';
										$no++;
													
										}
										if($value['grade'] != 'T') {
											$totalNH +=$value['NxH'];
											$totalSKS+=$value['jum_sks'];
										}
										$temp=$value['semester_ditempuh'];	
									}
									$ip = 0;
									if($totalNH !=0)			
										$ip = round($totalNH/$totalSKS, 2);
									$rows[]='
											<tr>
											<td colspan="7"><strong>Jumlah SKS : '.$totalSKS.'</strong></td>
											<td colspan="6"><strong>IP Semester : '.$ip.'</strong></td>
											</tr>';
								
									foreach($rows as $row)
									{
										echo $row;
									}
									?>
									
									<tbody>	
									</table>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				 
		 

		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script> 
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	  	
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		// var oTable1 = $('#sample-table-1').dataTable( {
		// "aoColumns": [
		  // { "bSortable": false },
		  // null, null,   null,   null,   null,  null, null,null,null,  
		  // { "bSortable": false, "sWidth": "20%" }
		// ] } );
		
		// var oTable2 = $('#sample-table-2').dataTable( {
		// "aoColumns": [
		  // { "bSortable": false },
		 // null, null,   null,   null,   null,  null, null,null, 
		  // { "bSortable": false, "sWidth": "20%" }
		// ] } );
		
		
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	