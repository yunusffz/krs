 

<script type="text/javascript">
	$(document).ready(function(){
		$("#datafrs").validate({
			debug: false,
			rules: {
				carimhs: "required"
			},
			messages: {
				carimhs: "<div style='width:670px; position:absolute; text-align:center; color:#fff; padding:5px; background-color:red;'>Masukkan NIM..!!!</div>",
			},
			submitHandler: function(form) {
				<?php 
					if($status=='0')
					{
						?>
						 //alert('sukses'); 
						 $.post('<?php echo base_url(); ?>dosen/setuju_krs', $("#datafrs").serialize(), function(data) {
							// $('#hasil').html(data);
							document.location.href = '<?=base_url().'dosen/persetujuan'?>';
						 });
						<?php
						 //header('location:'.base_url().'dosen/setuju_krs');
					}
					else
					{
						?>
						//alert('sukses'); 
						$.post('<?php echo base_url(); ?>dosen/batal_krs', $("#datafrs").serialize(), function(data) {
							//$('#hasil').html(data);
							document.location.href = '<?=base_url().'dosen/persetujuan'?>';
						});
						<?php
					}
				?>
			}
		});
	});
	</script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".delbutton").click(function(){
		 var element = $(this);
		 var del_id = element.attr("id");
		 var info = 'id=' + del_id;
		 if(confirm("Anda yakin akan menghapus?"))
		 {
			 $.ajax({
			 type: "POST",
			 url : "<?php echo base_url(); ?>dosen/hapus_krs/",
			 data: info,
			 success: function(){
				// colorbox.close();
				document.location.href = '<?=base_url().'dosen/persetujuan'?>';
				
			 }
			 });	
		 $(this).parents(".content").animate({ opacity: "hide" }, "slow");
 			}
		 return false;
		 });
	})
</script>

<?php
	if($status=='1')
	{
		$cls = "disetujui";
		$teks = "<b>Sudah Disetujui</b>";
	}
	else if($status=='0')
	{
		$cls = "";
		$teks = "<b>Belum Disetujui</b>";
	}
	else if($status==NULL)
	{
		$cls = "";
		$teks = "<b>Belum KRS</b>";
	}
?>
	<div id="hasil"></div>
<form name="datafrs" id="datafrs" method="POST" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse; font-size:12px;">
	<tr>
		<td width="100">NIM</td>
		<td width="250">:&nbsp;<input name="nim" value="<?php echo $nim; ?>" type="text" readonly="readonly"  size="30" style="font-size:12px;"/></td>
		<td>Semester, Tahun Ajaran</td>
		<td>:&nbsp;<input name="smstr_thn_ajaran" value="<?php echo $tahun_ajaran; ?>" type="text" readonly="readonly"  size="30" style="font-size:12px;" /></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>:&nbsp;<input name="nama_mhs" value="<?php echo $nama_mhs; ?>" type="text" readonly="readonly"   size="30" style="font-size:12px;"/></td>
		<td>IP Semester Lalu/Beban Study Maks</td>
		<td>:&nbsp;<input name="ip" value="<?php echo $ipk; ?>" type="text" size="10" readonly="readonly" style="font-size:12px;" />
		&nbsp;/&nbsp;<input name="beban_study" value="<?php echo $beban_studi; ?>" type="text" size="10" readonly="readonly" style="font-size:12px;" />
		</td>
				
	</tr>
	<tr>
		<td>Jurusan</td>
		<td>:&nbsp;<input name="jurusan" value="<?php echo $jurusan; ?>" type="text" readonly="readonly"  size="30" style="font-size:12px;" /></td>

		<td>Program Kelas</td>
		<td>:&nbsp;<input name="program" value="<?php echo $program; ?>" type="text" readonly="readonly"  size="30" style="font-size:12px;" />		
		</td>		
		
	</tr>
		<tr>
		<td>Dosen Wali</td>
		
		<td>
		:&nbsp;<input name="dosen_wali" value="<?php echo $dosen_wali; ?>" type="text" readonly="readonly"  size="30" style="font-size:12px;" />
		</td>

		<td>Semester yang akan ditempuh (*)</td>
		<td>:&nbsp;<input name="semester" value="<?php echo $smt_skr; ?>" type="text"  readonly="readonly"  size="30" style="font-size:12px;"/>
		</td>
	</tr>
	</table>
		
<div class="cleaner_h10"></div>

 
 
 
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
	<thead> 
	<tr>
		<th colspan="12" class="center" >
		<strong>Mata Kuliah yang Akan Ditempuh Pada Semester Ini :</strong></th>
	</tr>
	<tr>
		<th class="center">
			<label>
				<input type="checkbox" />
				<span class="lbl"></span>
			</label>
		</th>
		<th class="center">Kode MK</th>
		<th class="center">Mata Kuliah</th>
		<th class="center">Smstr</th>	
		<th class="center">SKS</th>
		<th class="center">Dosen</th>
		<th class="center">Kelas</th>
		<th class="center">Jadwal</th>
		<th class="center">Quota</th>
		<th class="center">Peserta</th>
		<th class="center">Calon Peserta</th>

		<?php
		if($status=='0')
		{
			echo '<th class="center">Batalkan</th>';
		}
		?>
	</tr>
	</thead> 
	<tbody>


<?php
	$state_app = 0;
	$no=1;
	$tot_sks = 0;
	foreach ($detailfrs->result_array() as $value) 
	{
	$tot_sks += $value['jum_sks'];
	if($value['kapasitas']==$value['Peserta'])
	{
		$state_app++;
		$color ="red";
	}
	else
	{
		$color ="";
	}
		
		echo '<tr bgcolor="'.$color.'" >
				
				<td>'.$value['kd_mk'].'</td>
				<td>'.$value['nama_mk'].'</td>
				<td>'.$value['semester'].'</td>
				<td>'.$value['jum_sks'].'</td>';
				
		echo '<td>'.$value['nama_dosen'].'</td>
				<td class="center">'.$value['kelas'].'</td>
				<td class="center">'.$value['jadwal'].'</td>
				<td class="center">'.$value['kapasitas'].'</td>
				<td class="center">'.$value['Peserta'].'</td>
				<td class="center">'.$value['CalonPeserta'].'</td>';
			if($status=='0')
			{
				echo '<td class="center">
				<a class="red delbutton" id="'.$value['nim'].'|'.$value['kd_jadwal'].'" href="#"><div id="box-link"><i class="icon-trash bigger-150"></i> </div></a>
				</td>';
			}
	}
	echo '<tr><td colspan=4>Total SKS Yang Akan Ditempuh :</td><td colspan=8 id="jmlcart"><b>'.$tot_sks.' SKS</b></td></tr>';
	echo '<tr><td colspan=4>Status Persetujuan KRS :</td><td colspan=8>'.$teks.'</td></tr>';
?>

	</tbody>
	</table>
<?php
	if($status=='0')
	{
		if($state_app < 1)
		{
			echo "<br>(+) Jika Anda menyetujui Rencana Study Mahasiswa yang bersangkutan silakan click tombol Setujui di bawah ini
		 <br><br><input type='submit' value='Setujui Kartu Rencana Studi' class='btn btn-info'>";
		}
		else if($state_app > 0)
		{
			echo "<p class='alert'>Anda tidak diperbolehkan menyetujui Kartu Rencana Studi ini, karena ada <b> ".$state_app." </b>mata kuliah yang telah terpenuhi kuotanya...!!!</p>";
		}
	}
	else{
		echo "<br>(+) Jika Anda ingin membatalkan seluruh Rencana Study Mahasiswa yang bersangkutan silakan click tombol Batalkan di bawah ini
		<br><br><input type='submit' class='btn btn-danger' value='Batalkan Kartu Rencana Studi'  >
		";
	}
?>	
	</form>
	 
	 