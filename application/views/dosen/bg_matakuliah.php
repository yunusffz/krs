			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Dosen</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Mata Kuliah</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Mata Kuliah yang Diampu
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid">
								<table border="0" cellpadding="5" cellspacing="5">
        <tbody><tr>
            <td>Minimum age:</td>
            <td><input id="tes" name="tes" type="text"></td>
        </tr>
    </tbody></table>
								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
								<thead> 
									<tr>   
										
										<th>No.</th>
										<th>Kode Mk</th>
										<th>Nama MataKuliah</th>
										<th>Kelas Program</th>
										<th>Detail MataKuliah</th>
									</tr>
								</thead>

								<tbody>	 
								<?php
									$no=1;
									foreach($jadwal->result_array() as $jd)
									{
										
										foreach ($thn_ajaran->result_array() as $thn) {
											
											if($jd['kd_tahun']==$thn['kd_tahun']){	 
												foreach ($matakuliah->result_array() as $mk) {
													if($mk['kd_mk'] == $jd['kd_mk']){
														echo'<tr>
																			
														<td class="center">'.$no.'</td><td>'.$mk['kd_mk'].'</td>
														<td>'.$mk['nama_mk'].'</td>'.
														'<td>'.$jd['kelas_program'].'</td>'.
														'<td><a href='.base_url().'dosen/detail_absen/'.$jd['kelas_program']
														.'/'.$mk['kd_mk'].'>
														<i class="icon-zoom-in bigger-130"></i></a></td>';
													
														echo '</tr>';
														$no++;	
													}
												}
												
											}
										}
									}
								?>
								</tbody>	 
								</table>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				
		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 	

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			minWidth:'60%',
			minHeight:'60%',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 		
 		
	    

		 
		var oTable1 = $('#sample-table-1').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  { "bSortable": true },
		  null, null,null, null,  null, null,null,  null, null, null,
		  { "bSortable": false}
		] } );
		
		$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
	        var kepro= $('#tes').val();
	        var kepro2 = data[2]; // use data for the age column
	 
	        if ((kepro && kepro2))
	        {
	            return true;
	        }
	        return true;
	    });

		var oTable2 = $('#sample-table-2').dataTable();
		
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	
 