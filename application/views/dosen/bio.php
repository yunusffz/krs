		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a href="<?=base_url()?>" class="brand">
						<small>
							<i class="icon-hospital"></i>
							Sistem Informasi Akademik Online STMIK Bandung
						</small>
					</a><!--/.brand-->

					<ul class="nav ace-nav pull-right">
						 
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?=base_url()?>assets/avatars/user1.png" alt="<?php echo $nama; ?>" />
								<span class="user-info">
									<small>Selamat Datang,</small>
									<?php echo $nama; ?>
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
								<li>
									<a href="#">
									Anda login sebagai <strong><?php echo $status; ?></strong>
									<br>
									dengan username <strong><?php echo $username; ?></strong>. 
									</a>
								</li>   
								
								<li class="divider"></li>
								
								<li>
									<a href="<?php echo base_url(); ?>dosen/akun">
										<i class="icon-cog"></i>
										Pengaturan Akun
									</a>
								</li>   
								
								<li class="divider"></li>
								<li>
									<a href="<?php echo base_url(); ?>web/logout">
										<i class="icon-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul><!--/.ace-nav-->
				</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>
