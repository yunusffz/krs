			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Dosen</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">
							<a href="<?php echo base_url();?>dosen/matakuliah">Mata Kuliah</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li class="active">
							Absensi
						</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Absensi Siswa
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							
							<?php 
								foreach ($mk->result_array() as $k) {
							?>
							<div class="span6">
								<table border="0"  cellpadding="5">
									<tr>
										<td width="100px">Kode Mata Kuliah</td>
										<td>:</td>
										<td>
											<?php echo $k['kd_mk']; ?>
										</td>
									</tr>
									<tr>
										<td>Mata Kuliah</td>
										<td>:</td>
										<td>
											<?php echo $k['nama_mk']; ?>
										</td>
									</tr>


									<tr>
										<td>Kelas Kuliah</td>
										<td>:</td>
										<td>
											<?php echo $kelas_program ?>
										</td>
									</tr>


									<tr>
										<td>SKS</td>
										<td>:</td>
										<td>
											<?php echo $k['jum_sks']; ?>
										</td>
									</tr>
								</table>
							</div>

							<div class="span6">
								<table border="0"  cellpadding="5">
									<tr>
										<td width="100px">Hari</td>
										<td>:</td>
										<td>
											<?php
												foreach ($jadwal->result_array() as $jd) {
													$hari = explode("/", $jd['jadwal'], 2);
													echo $hari[0];
												} 
											?>
										</td>
									</tr>
									<tr>

										<td>Jam</td>
										<td>:</td>
										<td>
											<?php
												foreach ($jadwal->result_array() as $jd) {
													$hari = explode("/", $jd['jadwal'], 3);
													echo $hari[1];
												} 
											?>
										</td>
									</tr>
									<tr>
										<td>Ruang</td>
										<td>:</td>
										<td>
											<?php
												foreach ($jadwal->result_array() as $jd) {
													$hari = explode("/", $jd['jadwal'], 3);
													echo $hari[2];
												} 
											?>
										</td>
									</tr>

								</table>
								<?php 
									}
								?>	
							</div>
						</div>	
						<div class="row"></div>
								<div class="row-fluid ">
									<div class="space-6"></div>
									<table id="table-1" class="table table-striped table-bordered table-hover" style="max-width:none !important;"> 
									<thead> 
										<tr>   
											<th rowspan=2 class="center">No.</th>
											<th rowspan=2 class="center">NIM</th>
											<th rowspan=2 class="center">Nama</th>
											<?php 
											foreach ($absen_dosen->result_array()as $abd) {
												echo '<th class="center">'.$abd['status'].'</td>';
											} 
											?>	
										</tr>
										<tr>
											<?php 
											foreach ($absen_dosen->result_array()as $abd) {
												$tgl = explode("-", $abd['tanggal'], 3);
												
												$d = $tgl[0];
												$m = $tgl[1];
												$y = $tgl[2];

												echo '<th class="center">'.$d.'<br>';
												switch ($m) {
													case '01':
														$m = 'Jan';
														break;
													case '02':
														$m = 'Feb';
														break;
													case '03':
														$m = 'Mar';
														break;
													case '04':
														$m = 'Apr';
														break;
													case '05':
														$m = 'Mei';
														break;
													case '06':
														$m = 'Jun';
														break;
													case '07':
														$m = 'Jul';
														break;
													case '08':
														$m = 'Agu';
														break;
													case '09':
														$m = 'Sep';
														break;
													case '10':
														$m = 'Okt';
														break;
													case '11':
														$m = 'Nov';
														break;
													case '12':
														$m = 'Des';
														break;
												}

												echo $m.'<br>'.$y.'</td>';
											} 
											?>
										</tr>									
									</thead>

									<tbody>	 
										<?php
											$no=1;
											
											foreach($mahasiswa->result_array() as $k)
											{
												$m=0;
												echo'<tr>
												
												<td class="center">'.$no.'</td><td class="center">'.$k['nim'].'</td>
												<td>'.$k['nama_mahasiswa'].'</td>';
													foreach ($absen_dosen->result_array()as $abd) {
														foreach ($absensi->result_array() as $ab) {

															if($ab['Nim'] == $k['nim'] && $ab['status']==$abd['status'])
																$m=1;
														
														}
														
														if($m==1){				
															echo'<td class="center">
															<img src="'.base_url().'assets/css/images/check.png" width="20px" height="20px"></img>
															</td>';
															$m=0;
														}
														else
															echo'<td class="center">
															<img src="'.base_url().'assets/css/images/cross.png" width="20px" height="20px"></img>
															</td>';
														
													}
												

												echo '</tr>';
												$no++;
												 
											}//*/	
										?>	
										<tfoot>
										<tr>
											<th colspan=3>Jumlah Kehadiran</td>
											<?php
												$no=1;
												foreach ($absen_dosen->result_array()as $abd) {
													$jml=0;
													foreach($mahasiswa->result_array() as $k)
													{
														$m=0;
														foreach ($absensi->result_array() as $ab) {

															if($ab['Nim'] == $k['nim'] && $ab['status']==$abd['status']){
																$m=1;
																$jml++;
															}
														
														}
														


													}
													echo'<th class="center">'.$jml.'</td>';
												}
											?>
										</tr>
										</tfoot>

									</tbody>	 
									</table>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				
		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			minWidth:'60%',
			minHeight:'60%',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 	
		var oTable1 = $('#table-1').dataTable({
			"bSort":false,
			"sScrollX": "100%",
			"bScrollCollapse" : true,
			"bPaginate":false
		}); 
		
		
	   

		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});



		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	
 