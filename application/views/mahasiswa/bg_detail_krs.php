			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Mahasiswa</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Kartu Rencana Studi</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Kartu Rencana Studi - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<p class="alert alert-block alert-success">
										Kartu Rencana Studi anda sudah disetujui. Untuk melakukan perubahan jadwal atau jadwal kuliah, silahkan hubungi dosen wali.</p>
										<form name="datafrs" id="datafrs" method="POST" action="<?php echo base_url(); ?>mahasiswa/simpan_krs">

									<table border="0" width="100%" cellpadding="7" cellspacing="0" style="border-collapse: collapse;">
									<tr>
										<td>NIM</td>
										<td><input name="nim" value="<?php echo $nim; ?>" type="text" readonly="readonly"  size="35" class="input-read-only"/></td>
										<td>Semester, Tahun Ajaran</td>
										<td><input name="smstr_thn_ajaran" value="22" type="text" readonly="readonly"  size="35" class="input-read-only" /></td>
									</tr>
									<tr>
										<td>Nama</td>
										<td><input name="nama_mhs" value="<?php echo $nama; ?>" type="text" readonly="readonly"   size="35" class="input-read-only"/></td>
										<td>IP Semester Lalu/Beban Study Maks</td>
										<td><input name="ip" value="<?php echo $ipk; ?>" type="text" size="10" readonly="readonly" class="input-read-only" />
										/ <input name="beban_study" value="24" type="text" size="10" readonly="readonly" class="input-read-only" />
										</td>
												
									</tr>
									<tr>
										<td>Jurusan</td>
										<td><input name="jurusan" value="<?php echo $jurusan; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" /></td>

										<td>Program Kelas</td>
										<td><input name="program" value="<?php echo $program; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" />		
										</td>		
										
									</tr>
										<tr>
										<td>Dosen Wali</td>
										<td>	
										<input name="dosen_wali" value="<?php echo $dosen_wali; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" />
										</td>

										<td>Semester yang akan ditempuh (*)</td>
										<td><input name="semester" value="<?php echo $smt_skr; ?>" type="text"  readonly="readonly"  size="35" class="input-read-only"/>
										</td>
									</tr>
									</table>
									
										<div class="cleaner_h10"></div>
										<!--
										<a href="print.php?print=laporan" target="_blank">Cetak Laporan</a>
										-->
										<div class="cleaner_h10"></div>
									
									<table id="sample-table-1" class="table table-striped table-bordered table-hover">
										<thead> 
											<tr>  
												
												<th align="center">Kode MK</th>
												<th align="center">Mata Kuliah</th>
												<th align="center">Smstr</th>	
												<th align="center">SKS</th>
												<th align="center">Dosen</th>
												<th align="center">Kelas</th>
												<th align="center">Jadwal</th>
												
												<?php
												if($status=='0')
												{
													echo '<th align="center">Batalkan</th>';
												}
												?> 
											</tr>
										</thead>

										<tbody>	
									
									


								<?php
									$state_app = 0;
									$no=1;
									$tot_sks = 0;
									foreach ($detailfrs->result_array() as $value) 
									{
									$tot_sks += $value['jum_sks'];
										
										echo '<tr>
												
												<td>'.$value['kd_mk'].'</td>
												<td>'.$value['nama_mk'].'</td>
												<td>'.$value['semester'].'</td>
												<td>'.$value['jum_sks'].'</td>';
												
										echo '<td>'.$value['nama_dosen'].'</td>
												<td align="center">'.$value['kelas'].'</td>
												<td align="center">'.$value['jadwal'].'</td>';
												
											
											if($status=='0')
											{
												echo '<td align="center">
												<a class="delbutton" id="'.$value['nim'].'|'.$value['kd_jadwal'].'" href="#"><div id="box-link">Batalkan</div></a>
												</td>';
											}
										echo "</tr>";
									}
									
								?>
									
									
										</tbody>	
									</table>
									
									
									<table id="sample-table-1" class="table table-striped table-bordered table-hover">
									<? echo '<tr><td colspan=4 align="right">Total SKS Yang Akan Ditempuh :</td><td colspan=8 id="jmlcart"><b>'.$tot_sks.' SKS</b></td></tr>'; ?>
									</table>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
  
	
	
		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-1').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,   null, null, null, null, null, null, null, 
		  null
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	