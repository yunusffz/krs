			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Mahasiswa</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Beranda</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Beranda - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<div class="widget-box">
										<div class="widget-header widget-header-flat">
											<h4 class="smaller">
												<i class="icon-leaf smaller-80"></i>
												Beranda - Sistem Informasi Akademik Online
											</h4>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<div class="row-fluid">
													<blockquote><strong>Selamat Datang di Sistem KRS Online STMIK BANDUNG.<br>
														Setelah melakukan login pertama kali ke Sistem Perwalian Intranet Online ini, <br>	
														harap segera melakukan pergantian password pada menu Pengaturan Akun, <br>	
														guna menanggulangi hal-hal yang tidak diinginkan.<br>	
														</strong> 
													</blockquote>
												</div>
										  
												<hr> 
  
												
												<ul>
													<li><strong>Kartu Rencana Studi</strong><br />Mengisi atau mengedit kartu rencana studi</li>
													<li><strong>Kartu Hasil Studi</strong><br />Melihat nilai per semester yang telah ditempuh</li>
													<li><strong>Cetak</strong><br />Mencetak Kartu Ujian Mandiri</li>
													<li><strong>Kalendar Akademik </strong><br />Melihat kalender Akademik</li>
													<li><strong>Info Kampus</strong><br />Melihat info seputar kampus dan perkuliahan</li>
													<li><strong>Log Out</strong><br />Keluar dari sistem perwalian online</li>
												</ul>
									 
											</div>
										</div>
									</div> 
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
 
				
		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			minWidth:'60%',
			minHeight:'60%',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-1').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  { "bSortable": true },
		  null, null,null, null,  null, null,null,  null, null, null,
		  { "bSortable": false}
		] } );
		
		var oTable2 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,    null,   null, null, 
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	
 