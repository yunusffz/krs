		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				 

				<ul class="nav nav-list">
					<li <? if($menu == 'beranda'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/">
							<i class="icon-home"></i>
							<span class="menu-text"> Beranda </span>
						</a>
					</li>
					
					<li <? if($menu == 'krs'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/krs">
							<i class="icon-file-alt"></i>
							<span class="menu-text"> Kartu Rencana Studi </span>
						</a>
					</li> 
					<li <? if($menu == 'khs'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/khs">
							<i class="icon-file-text-alt"></i>
							<span class="menu-text"> Kartu Hasil Studi </span>
						</a>
					</li> 
					
					
						<? 
						if (($menu['CetakKartuUts']=="active")or($menu['CetakKartuUas']=="active")){ 
							echo "<li class='active open'>"; 
						}else {
							echo "<li>";
						} ?>
					
							<a href="#" class="dropdown-toggle">
										<i class="icon-print"></i> 
										<span class="menu-text">
											Cetak
										</span> 
										<b class="arrow icon-angle-down"></b>
									</a>
							<ul class="submenu"> 
									
										<?php 
											
												if ($menu['CetakKartuUts']){ 
													echo "<li class='".$menu['CetakKartuUts']."'>"; 
												}else {
													echo "<li>";
												} 
													echo '<a href="'.base_url().'mahasiswa/'.'cetakUts">'; 
													echo '<i class="icon-print"></i>';
													echo '<span class="menu-text"> Cetak Kartu UTS </span>';
													echo '</a>'; 
												echo "</li>";
											
										?> 
										<?php 
									
										if ($menu['CetakKartuUas']){ 
											echo "<li class='".$menu['CetakKartuUas']."'>"; 
										}else {
											echo "<li>";
										} 
											echo '<a href="'.base_url().'mahasiswa/'.'cetakUas">'; 
											echo '<i class="icon-print"></i>';
											echo '<span class="menu-text"> Cetak Kartu UAS </span>';
											echo '</a>'; 
										echo "</li>";
									
								?>  
							</ul>
						</li>
					
					
					
					<li <? if($menu == 'kalendar'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/kalendar">
							<i class="icon-calendar"></i>
							<span class="menu-text"> Kalendar Akademik </span>
						</a>
					</li> 
					
					<li <? if($menu == 'info'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/info_kampus">
							<i class="icon-info"></i>
							<span class="menu-text"> Info Kampus </span>
						</a>
					</li> 
					<li <? if($menu == 'pengaturan'){echo "class='active'";} ?> >
						<a href="<?php echo base_url(); ?>mahasiswa/akun">
							<i class="icon-cog"></i>
							<span class="menu-text"> Pengaturan Akun </span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>web/logout">
							<i class="icon-off"></i>
							<span class="menu-text"> Keluar </span>
						</a>
					</li>
 
 
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
 