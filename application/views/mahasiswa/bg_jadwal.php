			<?php
				echo $bio;
				echo $menu;
			?>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Mahasiswa</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li> 
						<li class="active">Kartu Rencana Studi</li>
					</ul><!--.breadcrumb-->

					<div class="nav-search" id="nav-search">
						<form class="form-search" />
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="icon-search nav-search-icon"></i>
							</span>
						</form>
					</div><!--#nav-search-->
				</div>

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							Kartu Rencana Studi - Sistem Informasi Akademik Online
						</h1>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
 
								<div class="space-6"></div>
 
								<div class="row-fluid"> 
									<?php echo $this->session->flashdata('save_krs'); ?>
									<form name="datafrs" id="datafrs" method="POST" action="<?php echo base_url(); ?>mahasiswa/simpan_krs">

										<table border="0" width="100%" cellpadding="7" cellspacing="0" style="border-collapse: collapse;">
										<tr>
											<td>NIM</td>
											<td><input name="nim" value="<?php echo $nim; ?>" type="text" readonly="readonly"  size="35" class="input-read-only"/></td>
											<td>Semester, Tahun Ajaran</td>
											<td><input name="smstr_thn_ajaran" value="<?php echo $tahun_ajaran; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" /></td>
										</tr>
										<tr>
											<td>Nama</td>
											<td><input name="nama_mhs" value="<?php echo $nama; ?>" type="text" readonly="readonly"   size="35" class="input-read-only"/></td>
											<td>IP Semester Lalu/Beban Study Maks</td>
											<td><input name="ip" value="<?php echo $ipk; ?>" type="text" size="10" readonly="readonly" class="input-read-only" />
											/ <input name="beban_study" id="beban_study" value="22" type="text" size="10" readonly="readonly" class="input-read-only" />
											</td>
													
										</tr>
										<tr>
											<td>Jurusan</td>
											<td><input name="jurusan" value="<?php echo $jurusan; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" /></td>

											<td>Program Kelas</td>
											<td><input name="program" value="<?php echo $program; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" />		
											</td>		
											
										</tr>
											<tr>
											<td>Dosen Wali</td>
											<td>	
											<input name="dosen_wali" value="<?php echo $dosen_wali; ?>" type="text" readonly="readonly"  size="35" class="input-read-only" />
											</td>

											<td>Semester yang akan ditempuh (*)</td>
											<td><input name="semester" value="<?php echo $smt_skr; ?>" type="text"  readonly="readonly"  size="35" class="input-read-only"/>
											</td>
										</tr>
										</table>
											<table id="sample-table-1" class="table table-striped table-bordered table-hover">
											<thead> 
												<tr>
													<td colspan="13" align="center" bgcolor="#fff" style="text-transform:uppercase;"><strong>Mata Kuliah yang Akan Ditempuh Pada Semester Ini :</strong></td>
												</tr>
												<tr>
													<th class="center">
														Pilih</th>
													</th>
													<th align="center">Kode MK</th>
													<th align="center">Mata Kuliah</th>
													<th align="center">Smstr</th>	
													<th align="center">SKS</th>
													<th align="center" colspan="1">Dosen</th>
													<th align="center" colspan="1">Dosen</th>
													<th align="center">Kelas</th>
													<th align="center">Jadwal</th>
													<th align="center">Quota</th>
													<th align="center">Peserta</th>
													<th align="center">Calon Peserta</th> 
												</tr>
											</thead> 
													<?php Tampilkan_Mata_Kuliah($jadwal); ?>
											</table>
											
											<div class="space-6"></div>
											<?php Tampilkan_Detail_Frs($detail_krs); ?>	
											 
											<div class="space-6"></div>  
											<input type="hidden" name="cek" value="<?php echo $bc['kode']=$this->web_app_model->getAktifasi($nim); ?>">
											<?php 
												
												if ($bc['kode']=='AKTIF'){
													$status = 'Simpan Data Kartu Rencana Studi';
													$type = 'submit';
													$cf = "example_group";
													
												}
												else	{
													//$link = base_url().'keuangan/reg_aktivasi/'.$d['nim'];
													$status = 'Anda Belum Bisa Melakukan KRS';
													$type = 'reset';
													$cf = "";
													$link = '#';
												}
											echo '<p><input id="tombolsimpan" name="tombolsimpan" class="btn btn-info" type="'.$type.'" value="'.$status.'" disabled=true; /></p>';	
											?>
											
											
											
											<div class="space-6"></div>
											</form>
										</div>
										
									<?php
									 
									function Tampilkan_Detail_Frs($frsdetail){
										$valuedetailfrs='';
										$checkboxvalue='';
										$totalsks=0;
										foreach($frsdetail->result_array() as $value){
											if($valuedetailfrs == '')
												$valuedetailfrs .= $value['kd_jadwal'];
											else
												$valuedetailfrs .= "|".$value['kd_jadwal'];
											$checkboxvalue .="document.datafrs.chk_".$value['kd_mk']."_".$value['kd_jadwal'].".checked=true;\n";

											$totalsks += $value['jum_sks'];
										}
											
										echo '
										<div class="cleaner_h10"></div>
										<p class="left">
										<strong>Total Beban Study yang Akan Ditempuh </strong>
										<input id="idJumlahSKS" name="jumlahsks" value="'.$totalsks.'" type="text" size="2" style="background-color: #fff;" readonly="readonly"/>	
										<strong>SKS</strong>	
										</p>
										<p><input name="detailfrs" id="detailfrs" type="hidden" size=100 value="'.$valuedetailfrs.'"/></p>
										<script language="javascript">'.$checkboxvalue.'</script>'; 
									}
									
									 

									function Tampilkan_Mata_Kuliah($jdwl)
									{
										$rows=array();
										$index=0;
										$temp='';
										$acuan=0;
										$rowspan=1;
										foreach ($jdwl->result_array() as $value) 
										{ 
											if(($temp=='') || ($value['kd_mk']!=$temp)) {			
												$rows[$index] = '<tr>
													<td class="center">
														<label> 
															<input type="checkbox" id="chk_'.$value['kd_mk'].'_'.$value['kd_jadwal'].'" name="chk_'.$value['kd_mk'].'_'.$value['kd_jadwal'].'" value="ON" onchange="javascript:PilihMataKuliah2('.'\''.'chk_'.$value['kd_mk'].'_'.$value['kd_jadwal'].'\''.');" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center" rowspan="1">'.$value['kd_mk'].'</td>
													<td rowspan="1" id="'.'nama_'.$value['kd_mk'].'">'.$value['nama_mk'].'</td>
													<td align="center" rowspan="1">'.$value['semester'].'</td>
													<td align="center" rowspan="1" id="id'.$value['kd_mk'].'">'.$value['jum_sks'].'</td>';
													
													$rowspan=1;				
													$acuan=$index;
												}else if($value['kd_mk']==$temp) {
													$rows[$index] = '<tr>';
													$rowspan++;
												}

												$rows[$acuan]=str_replace('rowspan="'.($rowspan-1).'"', 'rowspan="'.$rowspan.'"', $rows[$acuan]);
												$peserta = isset($value['Peserta']) ? $value['Peserta']:'0';
												$calonpeserta = isset($value['CalonPeserta']) ? $value['CalonPeserta']:'0';
											
												$disabled ='';
												if($peserta >= $value['kapasitas'])
													$disabled ='disabled="disabled"';
												
												$linkpeserta = $peserta;
												if($peserta >0)
												$linkpeserta = '<a href="'.base_url().'mahasiswa/peserta/'.$value['kd_jadwal'].'_1
												/" title="Daftar Peserta Mata Kuliah '.$value['nama_mk'].'  -  Dosen '.$value['nama_dosen'].'" rel="example_group" class="badge badge-info cboxElement"  data-rel="colorbox">'
													.$peserta.'</a>';
													
												$linkcalonpeserta = $calonpeserta;
												if($calonpeserta >0)
												$linkcalonpeserta = '<a href="'.base_url().'mahasiswa/peserta/'.$value['kd_jadwal'].'_0
												/" title="Daftar Calon Peserta Mata Kuliah '.$value['nama_mk'].'  -  Dosen '.$value['nama_dosen'].'" rel="example_group" class="badge badge-info cboxElement"  data-rel="colorbox">	
												'.$calonpeserta.'</a>';
															
												$rows[$index] .='<td id="'.'cell_'.$value['kd_mk'].'_'.$value['kelas'].'">'.$value['kd_dosen'].'</td><td>'.$value['nama_dosen'].'</td>
													<td align="center">'.$value['kelas'].'</td>
													<td align="center" id="jdwl_'.$value['kd_jadwal'].'">'.$value['jadwal'].'</td>
													<td align="center">'.$value['kapasitas'].'</td>
													<td align="center">'.$linkpeserta.'</td>
													<td align="center">'.$linkcalonpeserta.'</td>
													 </tr>
													';
													
												$index++;
												$temp=$value['kd_mk'];
										}		
										foreach($rows as $row)
										{
											echo str_replace('rowspan="1"', '', $row);
										}
									}
									?>
								</div> 
 
								 
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid--> 
				</div><!--/.page-content--> 
				
				
				
				<!--/.footer--> 
				<div class="center alert alert-info">
					<div class="">
					Sistem Informasi Akademik (KRS) Online 2015 - TIM BTI STMIK BANDUNG 2015<br />
					Halaman ini dimuat selama <strong>{elapsed_time}</strong> detik 
					</div> 
				</div> 
				<!--/.footer--> 
				
				
			</div><!--/.main-content-->
  
<script languange="javascript">

var jumlahSKS=0;
var arrayDetail = new Array(); ;
	
/*
function PilihMataKuliah(chk) {
	var jumlahSKS=0;	
	var checkboxdipilih = chk.name;
	var beban=document.datafrs.beban_study.value;
	var temp=document.datafrs.jumlahsks.value;
	for(i=0 ; i<document.datafrs.length; i++) 
	{
		if(document.datafrs[i].value=="ON") 
		{
			if(document.datafrs[i].checked==true)
			{
				c1 = checkboxdipilih.split("_");
				c2 = document.datafrs[i].name.split("_");
				g1 = c1[2]+"|"+c1[2];
				g2 = c2[2]+"|"+c2[2];
				if(c1[1]==c2[1])
				{
					if(document.datafrs[i].name !=checkboxdipilih)
						document.datafrs[i].checked=false;
				}
			}
		}
	}

	for(i=0 ; i<document.datafrs.length; i++) 
	{
		if(document.datafrs[i].value=="ON") 
		{
			if(document.datafrs[i].checked==true)
			{
				parseData= document.datafrs[i].name.split("_");
				idSKS = "id"+parseData[1];
				jumlahSKS +=parseInt(document.getElementById(idSKS).innerHTML);
				if(jumlahSKS > beban) {
					chk.checked=false;
					jumlahSKS = temp;
					alert('Jumlah SKS yang anda ambil tidak boleh lebih dari beban maksimal');
				}
			}
		}
	}

	document.datafrs.jumlahsks.value=jumlahSKS;
	var detailfrs="";
	for(i=0 ; i<document.datafrs.length; i++) 
	{
		if(document.datafrs[i].value=="ON") 
		{
			parseData= document.datafrs[i].name.split("_");
			if(document.datafrs[i].checked==true)
			{
				if(detailfrs=="")
					detailfrs = parseData[2];
				else
					detailfrs += "|"+parseData[2];
			}
		}
	}	
	document.datafrs.detailfrs.value=detailfrs;
	if(parseInt(document.getElementById(idSKS).innerHTML)>0)
		document.datafrs.tombolsimpan.disabled=false;
	else
		document.datafrs.tombolsimpan.disabled=true;
}
*/

function PilihMataKuliah2(id) {
	
	// alert(id);
	
	var beban=document.getElementById('beban_study').value; 
	
	c1 = id.split("_");  
	var sks = document.getElementById('id'+c1[1]).innerHTML;  
	var chek = document.getElementById(id).checked; 
	
	if(chek == true)
	{  
		jumlahSKS = Number(jumlahSKS) + Number(sks); 
		if(jumlahSKS > beban) {
			document.getElementById(id).checked = false 
			jumlahSKS = Number(jumlahSKS) - Number(sks);
			alert('Jumlah SKS yang anda ambil tidak boleh lebih dari beban maksimal');
		}
		else
		{
			arrayDetail.push(c1[2]);
			document.getElementById('detailfrs').value = "";  
			for(var i = 0; i < arrayDetail.length; i++) { 
				if(document.getElementById("detailfrs").value == "") 
					document.getElementById("detailfrs").value =(arrayDetail[i]); 
				else 
					document.getElementById("detailfrs").value +="|"+(arrayDetail[i]);  
			}
		}
		document.getElementById('idJumlahSKS').value = jumlahSKS;  
	}
	else if(chek == false)
	{  
		jumlahSKS = Number(jumlahSKS) - Number(sks); 
		if(jumlahSKS > beban) {
			document.getElementById(id).checked = true 
			jumlahSKS = Number(jumlahSKS) + Number(sks);
			alert('Jumlah SKS yang anda ambil tidak boleh lebih dari beban maksimal');
		}  
		else
		{
			var z;	 
			for(z = 0; z < arrayDetail.length; z++) { 
				if(arrayDetail[z] == c1[2] )
				{ 
					break;
				}
			}
			arrayDetail.splice(z,1);  
			document.getElementById('detailfrs').value = "";  
			for(var i = 0; i < arrayDetail.length; i++) { 
				if(document.getElementById("detailfrs").value == "") 
					document.getElementById("detailfrs").value =(arrayDetail[i]); 
				else 
					document.getElementById("detailfrs").value +="|"+(arrayDetail[i]);  
			}
		}
		document.getElementById('idJumlahSKS').value = jumlahSKS;  
	}
			
	 
	 

	// document.datafrs.jumlahsks.value=jumlahSKS;
	// var detailfrs="";
	// for(i=0 ; i<document.datafrs.length; i++) 
	// {
		// if(document.datafrs[i].value=="ON") 
		// {
			// parseData= document.datafrs[i].name.split("_");
			// if(document.datafrs[i].checked==true)
			// {
				// if(detailfrs=="")
					// detailfrs = parseData[2];
				// else
					// detailfrs += "|"+parseData[2];
			// }
		// }
	// }	
	// document.datafrs.detailfrs.value=detailfrs;
	if(jumlahSKS>0)
		document.getElementById('tombolsimpan').disabled=false;
	else
		document.getElementById('tombolsimpan').disabled=true;
}
</script>


		<script src="<?=base_url()?>assets/js/jquery-2.0.3.min.js"> </script>
		
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url()?>assets/js/ace.min.js"></script>


		<script src="<?=base_url()?>assets/js/jquery.colorbox-min.js"></script>

	<script type="text/javascript">
 

	$(function() {
		var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="icon-arrow-left"></i>',
			next:'<i class="icon-arrow-right"></i>',
			close:'&times;',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
				document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
				document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon
 
	 
		var oTable1 = $('#sample-table-1').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,   null, null, null, null, null, null, null, null,  null 
		] } );
		
		var oTable2 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,   null,  
		  { "bSortable": false }
		] } );
		
	 
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	