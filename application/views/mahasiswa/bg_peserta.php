<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Included content</title>
	 
</head>
<body style="margin:5px;padding:5px">
 
 
<p>

	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead> 
			<tr>  
				<th class="center">
					<label>
						<input type="checkbox" />
						<span class="lbl"></span>
					</label>
				</th> 
				<th align="center">No</th>
				<th align="center">NIM</th>
				<th align="center">Nama</th>
				<th align="center">Jurusan</th>
				<th align="center">Status Persetujuan</th>
			</tr>
		</thead>

		<tbody>	
	 
<?php
$no=1;
foreach($peserta->result_array() as $value){
	 
	$st = "";
			if($value['status']=='0'){ 
				$st = "Belum Disetujui"; 
			} 
			else {
				$st = "Sudah Disetujui"; 
			}
	echo '<tr>
	<td class="center">
		<label>
			<input type="checkbox" />
			<span class="lbl"></span>
		</label>
	</td>
	<td >'.$no.'</td>
	<td >'.$value['nim'].'</td>
	<td >'.$value['nama_mahasiswa'].'</td>
	<td >'.$value['jurusan'].'</td>
	<td >'.$st.'</td>
	</tr>';
	$no++;
}
?>	
		</tbody>	
</table>
</p>

  
</body>
</html>


	<script type="text/javascript">
 

	$(function() {
		   
		
		var oTable2 = $('#sample-table-2').dataTable( {
		"aoColumns": [
		  { "bSortable": false },
		  null, null,    null,   null, null, 
		] } );
		
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
	
	 
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
	
			var off2 = $source.offset();
			var w2 = $source.width();
	
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		
		 
	})
	 
	</script>	