<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 * @keterangan : Controller untuk halaman awal ketika aplikasi krs web based diakses
	 **/
	public function _construct()
	{
		session_start();
	}
	
	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			$d['judul'] = "Login - Sistem Informasi Akademik Online";
			$frm['judul'] = "Login - Sistem Informasi Akademik Online";
			$frm['message'] = "";
			
			//buat atribut form
			$frm['username'] = array('name' => 'username',
				'id' => 'username',
				'type' => 'text',
				'value' => '',
				'class' => 'input-teks-login',
				'placeholder' => 'Masukkan username.....'
			);
			$frm['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'value' => '',
				'class' => 'input-teks-login',
				'placeholder' => 'Masukkan password.....'
			);
			
			// $this->load->view('global/bg_top',$d);
			$this->load->view('web/bg_login',$frm);
			// $this->load->view('global/bg_footer',$d);
		}
		else
		{
			$st = $this->session->userdata('stts');
			if($st=='mahasiswa')
			{
				header('location:'.base_url().'mahasiswa');
			}
			else if($st=='dosen')
			{
				header('location:'.base_url().'dosen');
			}
			else if($st=='admin')
			{
				header('location:'.base_url().'admin');
			}
			else if($st=='keuangan')
			{
				header('location:'.base_url().'keuangan');
			}
		
		}
	}
	
	
	public function fail()
	{
		$d['judul'] = "Login - Sistem Informasi Akademik Online";
		$frm['judul'] = "Login - Sistem Informasi Akademik Online";
		$this->load->view('web/bg_login',$frm);
	}
	
	
	public function login()
	{
		$d['judul'] = "Login - Sistem Informasi Akademik Online";
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u,$p);
	}
	
	public function logout()
	{
		$cek = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'web');
		}
		else
		{
			$this->session->sess_destroy();
			header('location:'.base_url().'web');
		}
	}

	public function forgotpassword()
	{
		$frm['judul'] = "Login - Sistem Informasi Akademik Online";
		$frm['hint'] = $this->web_app_model->getAllData('tbl_hint');
		$this->load->view('web/bg_forgotpassword',$frm);	
	}


		function generatePassword ($length = 8)
	{

	  // start with a blank password
	  $password = "";

	  // define possible characters - any character in this string can be
	  // picked for use in the password, so if you want to put vowels back in
	  // or add special characters such as exclamation marks, this is where
	  // you should do it
	  $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

	  // we refer to the length of $possible a few times, so let's grab it now
	  $maxlength = strlen($possible);

	  // check for length overflow and truncate if necessary
	  if ($length > $maxlength) {
	    $length = $maxlength;
	  }

	  // set up a counter for how many characters are in the password so far
	  $i = 0; 

	  // add random characters to $password until $length is reached
	  while ($i < $length) { 

	    // pick a random character from the possible ones
	    $char = substr($possible, mt_rand(0, $maxlength-1), 1);

	    // have we already used this character in $password?
	    if (!strstr($password, $char)) { 
	      // no, so it's OK to add it onto the end of whatever we've already got...
	      $password .= $char;
	      // ... and increase the counter by one
	      $i++;
	    }

	  }

	  // done!
	  return $password;

	}

	public function do_forgotpassword()
	{
		$data['username'] = $this->input->post('username');
		$data['id_hint'] = $this->input->post('id_hint');
		$data['answer_hint'] = $this->input->post('hint_answer');
		
		$conf = $this->web_app_model->getForgotPassword($data);

		if($conf == false){
			$frm['judul'] = "Login - Sistem Informasi Akademik Online";
			$this->load->view('web/bg_login',$frm);
		}else{
			$password = $this->generatePassword();
			$passwordb = array('password'=>md5($password));
			$this->web_app_model->updateData('tbl_login',$passwordb,$data['username'],'username');
			$config = Array(
			    'protocol' => 'smtp',
			    'smtp_host' => 'ssl://smtp.googlemail.com',
			    'smtp_port' => 465,
			    'smtp_user' => 'ddolffz@gmail.com',
			    'smtp_pass' => 'tesffzddol',
			    'mailtype'  => 'html', 
			    'charset'   => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			$this->email->from('ddolffz@gmail.com', 'dodo');
			foreach ($conf ->result_array() as $value) {
				$email = $value['email'];
			}
	        $this->email->to($email); 

	        $this->email->subject('Email Test');
	        $this->email->message('Password baru anda : '.$password);  
			$result = $this->email->send();

			$this->session->set_flashdata('message2', 'Password baru anda sudah dikirim ke email anda. Terima Kasih.'); 
			header('location:'.base_url().'web');
	
		}
	}

}
/* End of file web.php */
/* Location: ./application/controllers/web.php */