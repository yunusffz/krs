<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keuangan extends CI_Controller {


	
	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Beranda - Sistem Informasi Akademik Online - Keuangan";
			$mn['menu'] = "beranda";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_home',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function persetujuan()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Persetujuan Administrasi - Sistem Informasi Akademik Online";
			$mn['menu'] = "persetujuan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['tahun']=$this->web_app_model->getKodeTahunAjaran();
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getMahasiswaKBB($bc['kd_staff'],$bc['tahun']);
			
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_persetujuan',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "info";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$bc['info'] = $this->web_app_model->getAllDataLimited_Order_DESC('tbl_info',$offset,$limit);
			$tot_hal =  $this->web_app_model->getAllData('tbl_info');
			$config['base_url'] = base_url() . 'keuangan/info/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$bc["paginator"] =$this->pagination->create_links();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_info',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function edit_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['info'] = $this->web_app_model->getSelectedData('tbl_info','kd_info',$this->uri->segment(3));
			$this->load->view('keuangan/bg_edit_info',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function hapus_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$id = $this->uri->segment(3);
			$hapus = array('kd_info' => $id);
			$this->web_app_model->deleteData('tbl_info',$hapus);
			header('location:'.base_url().'keuangan/info');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function tambah_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$this->load->view('keuangan/bg_tambah_info');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function simpan_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$st = $this->input->post("stts");
			
			$simpan["judul"] = $this->input->post("judul");
			$simpan["isi"] = $this->input->post("isi");
			
			if($st=="edit")
			{
				$kd_info = $this->input->post('kd_info');
				$where = array('kd_info'=>$kd_info);
				$this->web_app_model->updateDataMultiField("tbl_info",$simpan,$where);
				?>
					<!--
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				header('location:'.base_url().'keuangan/info');
			}
			else if($st=="tambah")
			{
				$simpan["waktu_post"] = strtotime(date('Y-m-d H:i:s'));
				$this->web_app_model->insertData('tbl_info',$simpan);
				?>
					<!--
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				header('location:'.base_url().'keuangan/info');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	
	public function akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "pengaturan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_akun',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$username = $this->session->userdata('kd_staff');
			$pass_lama = $this->input->post('pass_lama');
			$pass_baru = $this->input->post('pass_baru');
			$ulangi_pass = $this->input->post('ulangi_pass');
			
			$data['username'] = $username;
			$data['password'] = md5($pass_lama);
			$cek = $this->web_app_model->getSelectedDataMultiple('tbl_login',$data);
			if($cek->num_rows()>0)
			{
				if($pass_baru==$ulangi_pass)
				{
					$simpan['password'] = md5($pass_baru);
					$where = array('username'=>$username);
					$this->web_app_model->updateDataMultiField("tbl_login",$simpan,$where);
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Berhasil mengubah password</p>");
					header('location:'.base_url().'keuangan/akun');
				}
				else
				{
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Password Tidak Sama</p>");
					header('location:'.base_url().'keuangan/akun');
				}
			}
			else
			{
				$this->session->set_flashdata("save_akun","
				<p class='alert alert-block alert-success'>
				Password Lama Salah</p>");
				header('location:'.base_url().'keuangan/akun');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function aktivasi()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$this->load->view('keuangan/bg_aktivasi');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	// aktivasi
	public function reg_aktivasi()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['nim'] = $this->web_app_model->getSelectedData('tbl_mahasiswa','nim',$this->uri->segment(3));
			$bc['ket']=$this->web_app_model->getTahunAjaran();
			$this->load->view('keuangan/bg_aktivasi',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function reg_aktivasiUTS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['nim'] = $this->web_app_model->getSelectedData('tbl_mahasiswa','nim',$this->uri->segment(3));
			$bc['ket']=$this->web_app_model->getTahunAjaran();
			$this->load->view('keuangan/bg_aktivasiUTS',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function reg_aktivasiUAS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['nim'] = $this->web_app_model->getSelectedData('tbl_mahasiswa','nim',$this->uri->segment(3));
			$bc['ket']=$this->web_app_model->getTahunAjaran();
			$this->load->view('keuangan/bg_aktivasiUAS',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	// simpan Aktivasi
	public function simpan_aktivasi()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$st = $this->input->post("stts");
			
			$simpan["nim"] = $this->input->post("nim");
			$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
			$simpan["Tgl_reg"] = $this->input->post("tgl");
			$simpan["aktif"] = $this->input->post("aktif");
			$simpan["kwitansi"] = $this->input->post("kwitansi");
			$simpan["log"] = $this->input->post("logstt");
			
			if($st=="edit")
			{
				$nim = $this->input->post('nim');
				$where = array('nim'=>$nim);
				$this->web_app_model->updateDataMultiField("tbl_regmhs",$simpan,$where);
				
				header('location:'.base_url().'keuangan/persetujuan');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			 if($st=="tambah")
			{
				$simpan["nim"] = $this->input->post("nim");
				$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
				if($this->web_app_model->cekAktivasiMax($simpan["nim"])==0)
				{
				

						$this->web_app_model->insertData('tbl_regmhs',$simpan);
				
				header('location:'.base_url().'keuangan/persetujuan');
				
				}
	
				else
				{
					$this->session->set_flashdata("save_dosen","
					<p class='alert alert-block alert-success'>
					Data Sudah Ada</p>");
				header('location:'.base_url().'keuangan/persetujuan');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_aktivasiUTS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$st = $this->input->post("stts");
			
			$simpan["nim"] = $this->input->post("nim");
			$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
			$simpan["Tgl_regUTS"] = $this->input->post("tgl");
			$simpan["uts"] = $this->input->post("aktif");
			$simpan["kwitansiUTS"] = $this->input->post("kwitansi");
			$simpan["logUTS"] = $this->input->post("logstt");
			
			if($st=="edit")
			{
				$nim = $this->input->post('nim');
				$where = array('nim'=>$nim);
				$this->web_app_model->updateDataMultiField("tbl_regmhs",$simpan,$where);
				
				header('location:'.base_url().'keuangan/persetujuanUTS');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			 if($st=="tambah")
			{
				$simpan["nim"] = $this->input->post("nim");
				$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
				if($this->web_app_model->cekAktivasiMax($simpan["nim"])==0)
				{
				

						$this->web_app_model->insertData('tbl_regmhs',$simpan);
				
				header('location:'.base_url().'keuangan/persetujuanUTS');
				
				}
	
				else
				{
					$this->session->set_flashdata("save_dosen","
					<p class='alert alert-block alert-success'>
					Data Sudah Ada</p>");
				header('location:'.base_url().'keuangan/persetujuanUTS');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_aktivasiUAS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$st = $this->input->post("stts");
			
			$simpan["nim"] = $this->input->post("nim");
			$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
			$simpan["Tgl_regUAS"] = $this->input->post("tgl");
			$simpan["uas"] = $this->input->post("aktif");
			$simpan["kwitansiUAS"] = $this->input->post("kwitansi");
			$simpan["logUAS"] = $this->input->post("logstt");
			
			if($st=="edit")
			{
				$nim = $this->input->post('nim');
				$where = array('nim'=>$nim);
				$this->web_app_model->updateDataMultiField("tbl_regmhs",$simpan,$where);
				
				header('location:'.base_url().'keuangan/persetujuanUAS');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			 if($st=="tambah")
			{
				$simpan["nim"] = $this->input->post("nim");
				$simpan["Tahun_Ajaran"] = $this->input->post("tahun");
				if($this->web_app_model->cekAktivasiMax($simpan["nim"])==0)
				{
				

						$this->web_app_model->insertData('tbl_regmhs',$simpan);
				
				header('location:'.base_url().'keuangan/persetujuanUAS');
				
				}
	
				else
				{
					$this->session->set_flashdata("save_dosen","
					<p class='alert alert-block alert-success'>
					Data Sudah Ada</p>");
				header('location:'.base_url().'keuangan/persetujuanUAS');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	//untuk reg Ulang
	public function edit_reg()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['reg'] = $this->web_app_model->getSelectedData('tbl_regmhs','nim',$this->uri->segment(3));
			
			$this->load->view('keuangan/bg_edit_reg',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_regUTS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['reg'] = $this->web_app_model->getSelectedData('tbl_regmhs','nim',$this->uri->segment(3));
			
			$this->load->view('keuangan/bg_edit_regUTS',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function edit_regUAS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$bc['reg'] = $this->web_app_model->getSelectedData('tbl_regmhs','nim',$this->uri->segment(3));
			
			$this->load->view('keuangan/bg_edit_regUAS',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	////
	public function persetujuanUTS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Persetujuan Administrasi - Sistem Informasi Akademik Online";
			$mn['menu'] = "persetujuan UTS";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getMahasiswaUTS($bc['kd_staff']);
			
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_uts',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function persetujuanUAS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='keuangan')
		{
			$d['judul'] = "Persetujuan Administrasi - Sistem Informasi Akademik Online";
			$mn['menu'] = "persetujuan UAS";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kd_staff'] = $this->session->userdata('kd_staff');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('keuangan/menu', $mn, true);
			$bc['bio'] = $this->load->view('keuangan/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getMahasiswaUAS($bc['kd_staff']);
			
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('keuangan/bg_uas',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
}

/* End of file KEUANGAN.php */
/* Location: ./application/controllers/keuangan.php */