<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 * @keterangan : Controller untuk halaman khusus admin
	 */
	
	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Beranda - Sistem Informasi Akademik Online";
			$mn['menu'] = "beranda";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_home',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tampil_jadwal()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Manajemen Jadwal - Sistem Informasi Akademik Online";
			$mn['menu'] = "jadwal";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['jadwal'] = $this->web_app_model->getSemuaJadwal();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_jadwal',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	
	public function tampil_jadwalUTS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Manajemen Jadwal UTS - Sistem Informasi Akademik Online";
			$mn['menu'] = "jadwal UTS";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['jadwal'] = $this->web_app_model->getSemuaJadwalUTS();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_uts',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tampil_jadwalUAS()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Manajemen Jadwal UAS - Sistem Informasi Akademik Online";
			$mn['menu'] = "jadwal UAS";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['jadwal'] = $this->web_app_model->getSemuaJadwalUAS();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_uas',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_jadwal()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$id = $this->uri->segment(3);
			$hapus = array('kd_jadwal' => $id);
			$this->web_app_model->deleteData('tbl_jadwal',$hapus);
			header('location:'.base_url().'admin/tampil_jadwal');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function peserta()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d = explode("_",$this->uri->segment(3));
			$bc['peserta'] = $this->web_app_model->getPeserta($d[0],$d[1]);
			
			$this->load->view('admin/bg_peserta',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function peserta_print()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d = explode("_",$this->uri->segment(3));
			$peserta = $this->web_app_model->getPeserta($d[0],$d[1]);
			$peserta_array = $this->web_app_model->getPesertaPrint($d[0],$d[1])->row_array(1);
			 
			$jadwal =  explode("/", $peserta_array['jadwal']);
			$peserta_jumlah = $peserta->num_rows();
			$jam = explode('-',$jadwal[1]);
			// echo $jadwal[0];
			// echo $jadwal[1];
			// echo $jadwal[2];
			// echo $peserta_jumlah;
			
			$this->load->helper('konversi'); 
			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
				
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			
			
			//load our new PHPExcel library
			$this->load->library('excel'); 
			$this->excel->setActiveSheetIndex(0); 
			$this->excel->getActiveSheet()->setTitle('Daftar Hadir');
			
			$styleBorderAll = array(
			  'borders' => array(
				'allborders' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN,
				)
			  )
			);	
			
			$styleBorderThin = array(
			  'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN,
				)
			  )
			);			
			
			$styleBorderDouble = array(
			  'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
				)
			  )
			);
			
			$styleAlignmentLeft = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				)
			);
			
			$styleAlignmentCenter = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				)
			);

			
			$this->excel->getActiveSheet()->setCellValue('A1', 'DAFTAR HADIR MAHASISWA DAN DOSEN');
			$this->excel->getActiveSheet()->mergeCells('A1:T1');
			
			$this->excel->getActiveSheet()->getStyle('A1:T1')->applyFromArray($styleBorderThin);
			unset($styleArray);
			
			$this->excel->getActiveSheet()->setCellValue('A2', 'SEMESTER '.$peserta_array['keterangan']);//
			$this->excel->getActiveSheet()->mergeCells('A2:T2');
			
			$this->excel->getActiveSheet()->setCellValue('A3', 'PROGRAM STUDI : '. $peserta_array['kode_jur'] .' PROGRAM PENDIDIKAN : SARJANA (S1)');//
			$this->excel->getActiveSheet()->mergeCells('A3:T3'); 
			
			$this->excel->getActiveSheet()->getStyle("A1:T3")->applyFromArray($styleAlignmentCenter);
			
			$this->excel->getActiveSheet()->setCellValue('A5', 'Kode MK'); 
			$this->excel->getActiveSheet()->mergeCells('A5:B5'); 
			$this->excel->getActiveSheet()->setCellValue('C5', ':');  
			$this->excel->getActiveSheet()->setCellValue('D5', $peserta_array['kd_mk']);  //
			
			$this->excel->getActiveSheet()->setCellValue('A6', 'Nama MK'); 
			$this->excel->getActiveSheet()->mergeCells('A6:B6');
			$this->excel->getActiveSheet()->setCellValue('C6', ':');  
			$this->excel->getActiveSheet()->setCellValue('D6', $peserta_array['nama_mk']);  //
			
			$this->excel->getActiveSheet()->setCellValue('A7', 'SKS'); 
			$this->excel->getActiveSheet()->mergeCells('A7:B7');
			$this->excel->getActiveSheet()->setCellValue('C7', ':');  
			$this->excel->getActiveSheet()->setCellValue('D7', $peserta_array['jum_sks']);  //
			
			$this->excel->getActiveSheet()->setCellValue('A8', 'Kelas Kuliah'); 
			$this->excel->getActiveSheet()->mergeCells('A8:B8');
			$this->excel->getActiveSheet()->setCellValue('C8', ':');  
			$this->excel->getActiveSheet()->setCellValue('D8', $peserta_array['kelas_program'] .' - '. $peserta_array['kelas']);  //
			
			$this->excel->getActiveSheet()->setCellValue('A9', 'Nama Dosen'); 
			$this->excel->getActiveSheet()->mergeCells('A9:B9');
			$this->excel->getActiveSheet()->setCellValue('C9', ':'); 
			$this->excel->getActiveSheet()->setCellValue('D9', $peserta_array['nama_dosen']);  //
			
			$this->excel->getActiveSheet()->setCellValue('G5', 'Hari'); 
			$this->excel->getActiveSheet()->mergeCells('G5:J5');
			$this->excel->getActiveSheet()->setCellValue('G6', $jadwal[0]); //
			$this->excel->getActiveSheet()->mergeCells('G6:J6');
			
			$this->excel->getActiveSheet()->setCellValue('K5', 'Jam'); 
			$this->excel->getActiveSheet()->mergeCells('K5:O5');
			$this->excel->getActiveSheet()->setCellValue('K6', $jam[0]); //
			$this->excel->getActiveSheet()->mergeCells('K6:L6'); 
			$this->excel->getActiveSheet()->setCellValue('M6', 's/d'); //
			$this->excel->getActiveSheet()->setCellValue('N6', $jam[1]); //
			$this->excel->getActiveSheet()->mergeCells('N6:O6'); 
			
			$this->excel->getActiveSheet()->setCellValue('P5', 'Ruang'); 
			$this->excel->getActiveSheet()->mergeCells('P5:S5');
			$this->excel->getActiveSheet()->setCellValue('P6', $jadwal[2]); //
			$this->excel->getActiveSheet()->mergeCells('P6:S6');
			
			$this->excel->getActiveSheet()->setCellValue('M9', 'Jumlah Peserta'); 
			$this->excel->getActiveSheet()->mergeCells('M9:Q9');
			$this->excel->getActiveSheet()->setCellValue('R9', ':'); 
			$this->excel->getActiveSheet()->setCellValue('S9', $peserta_jumlah); //
			
			$this->excel->getActiveSheet()->getStyle('G5:S6')->applyFromArray($styleBorderAll);
			unset($styleArray);
			
			$this->excel->getActiveSheet()->getStyle("G5:S9")->applyFromArray($styleAlignmentCenter);
			
			$this->excel->getActiveSheet()->getStyle('A10:T10')->applyFromArray($styleBorderDouble);
			unset($styleArrayDouble);
			
			$this->excel->getActiveSheet()->setCellValue('A12', 'No'); 
			$this->excel->getActiveSheet()->mergeCells('A12:A13');
			$this->excel->getActiveSheet()->setCellValue('B12', 'NIM'); 
			$this->excel->getActiveSheet()->mergeCells('B12:B13');
			$this->excel->getActiveSheet()->setCellValue('C12', ''); 
			$this->excel->getActiveSheet()->mergeCells('C12:C13');
			$this->excel->getActiveSheet()->setCellValue('D12', 'Nama'); 
			$this->excel->getActiveSheet()->mergeCells('D12:D13');
			$this->excel->getActiveSheet()->setCellValue('E12', 'Pertemuan'); 
			$this->excel->getActiveSheet()->mergeCells('E12:T12');
			$this->excel->getActiveSheet()->setCellValue('E13', '1'); 
			$this->excel->getActiveSheet()->setCellValue('F13', '2'); 
			$this->excel->getActiveSheet()->setCellValue('G13', '3'); 
			$this->excel->getActiveSheet()->setCellValue('H13', '4'); 
			$this->excel->getActiveSheet()->setCellValue('I13', '5'); 
			$this->excel->getActiveSheet()->setCellValue('J13', '6'); 
			$this->excel->getActiveSheet()->setCellValue('K13', '7'); 
			$this->excel->getActiveSheet()->setCellValue('L13', '8'); 
			$this->excel->getActiveSheet()->setCellValue('M13', '9'); 
			$this->excel->getActiveSheet()->setCellValue('N13', '10'); 
			$this->excel->getActiveSheet()->setCellValue('O13', '11'); 
			$this->excel->getActiveSheet()->setCellValue('P13', '12'); 
			$this->excel->getActiveSheet()->setCellValue('Q13', '13'); 
			$this->excel->getActiveSheet()->setCellValue('R13', '14'); 
			$this->excel->getActiveSheet()->setCellValue('S13', '15'); 
			$this->excel->getActiveSheet()->setCellValue('T13', '16'); 
			 
			$this->excel->getActiveSheet()->getStyle("A12:T13")->applyFromArray($styleAlignmentCenter);
			 
			foreach(range('E','T') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)
					->setWidth(3);
			}
			
			$i=13;
			foreach($peserta->result() as $row) 
			{ 
			$i++;
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $i-13); 
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $row->nim); 
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $row->nama_mahasiswa);  
				 
			} 
			 
			$j = $i+1;
			$k = $i+2;
			$l = $i+4;
			$m = $i+5;
			$n = $i+6;
			$this->excel->getActiveSheet()->setCellValue('A'.$j, 'Jumlah Mahasiswa yang hadir'); 
			$this->excel->getActiveSheet()->mergeCells('A'.$j.':'.'D'.$j.':');
			$this->excel->getActiveSheet()->setCellValue('A'.$k, 'Paraf Dosen'); 
			$this->excel->getActiveSheet()->mergeCells('A'.$k.':'.'D'.$k.':');
			$this->excel->getActiveSheet()->setCellValue('A'.$l, 'Dicetak pada : ' . tgl_jam_indo($datetime->format('Y-m-d') . ' ' .  $datetime->format("H:i:s"))); 
			$this->excel->getActiveSheet()->mergeCells('A'.$l.':'.'E'.$l.':');
			$this->excel->getActiveSheet()->setCellValue('A'.$m, 'Catatan : Daftar Hadir Mahasiswa dan Dosen ini'); 
			$this->excel->getActiveSheet()->mergeCells('A'.$m.':'.'E'.$m.':');
			$this->excel->getActiveSheet()->setCellValue('A'.$n, 'harus selalu diisi pada setiap perkuliahan.'); 
			$this->excel->getActiveSheet()->mergeCells('A'.$n.':'.'E'.$n.':');
			 
			$this->excel->getActiveSheet()->getStyle('A12:T'.$k)->applyFromArray($styleBorderAll);
			unset($styleArray);
			 
			
			$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			 
			$this->excel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleAlignmentLeft);
			  
			
			$filename = 'DAFTAR HADIR MAHASISWA DAN DOSEN - '. $peserta_array['nama_mk'] . '.xls';
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
						 
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	 
			// exit;
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_jadwal()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['mata_kuliah'] = $this->web_app_model->getAllData('tbl_mk');
			$bc['dosen'] = $this->web_app_model->getAllData('tbl_dosen');
			$bc['tahun_ajaran'] = $this->web_app_model->getSelectedData('tbl_thn_ajaran','stts',1);
			$bc['edit'] = $this->web_app_model->getEditJadwal($this->uri->segment('3'));
			
			$this->load->view('admin/bg_edit_jadwal',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tambah_jadwal()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['mata_kuliah'] = $this->web_app_model->getAllDataMatKul('tbl_mk');
			$bc['dosen'] = $this->web_app_model->getAllData('tbl_dosen');
			$bc['tahun_ajaran'] = $this->web_app_model->getSelectedData('tbl_thn_ajaran','stts',1);
			$bc['jam'] = $this->web_app_model->getAllData('tbl_jam');
				$bc['kelas'] = $this->web_app_model->getAllData('tbl_kelas');
			$this->load->view('admin/bg_tambah_jadwal',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_jadwal()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$hari = $this->input->post('hari');
			$mulai = clear($this->input->post('jam_mulai'));
			$akhir = clear($this->input->post('jam_akhir'));
			$ruangan = clear($this->input->post('ruang'));
			$jadwal = $hari.' / '.$mulai.'-'.$akhir.' / '.$ruangan;
			
			$simpan["kd_mk"] = $this->input->post("kd_mk");
			$simpan["kd_dosen"] = $this->input->post("kd_dosen");
			$simpan["kd_tahun"] = $this->input->post("kd_tahun");
			$simpan["jadwal"] = $jadwal;
			$simpan["kapasitas"] = $this->input->post("kapasitas");
			$simpan["kelas_program"] = $this->input->post("kelas_program");
			$simpan["kelas"] = $this->input->post("kelas");
			
			if($st=="edit")
			{
				$kd_jadwal = $this->input->post('kd_jadwal');
				$where = array('kd_jadwal'=>$kd_jadwal);
				$this->web_app_model->updateDataMultiField("tbl_jadwal",$simpan,$where);
				
				header('location:'.base_url().'admin/tampil_jadwal');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$this->web_app_model->insertData('tbl_jadwal',$simpan);
				header('location:'.base_url().'admin/tampil_jadwal');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Dosen - Sistem Informasi Akademik Online";
			$mn['menu'] = "dosen";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['dosen'] = $this->web_app_model->getAllData('tbl_dosen');
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_dosen',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function staff()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar staff - Sistem Informasi Akademik Online";
			$mn['menu'] = "staff";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['staff'] = $this->web_app_model->getAllData('tbl_staff');
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_staff',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tambah_staff()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$this->load->view('admin/bg_tambah_staff');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function tambah_dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$this->load->view('admin/bg_tambah_dosen');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['dosen'] = $this->web_app_model->getSelectedData('tbl_dosen','kd_dosen',$this->uri->segment(3));
			
			$this->load->view('admin/bg_edit_dosen',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$simpan["nidn"] = $this->input->post("nidn");
			$simpan["nama_dosen"] = $this->input->post("nama_dosen");
			
			if($st=="edit")
			{
				$kd_dosen = $this->input->post('kd_dosen');
				$where = array('kd_dosen'=>$kd_dosen);
				$this->web_app_model->updateDataMultiField("tbl_dosen",$simpan,$where);
				
				header('location:'.base_url().'admin/dosen');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$simpan["kd_dosen"] = $this->input->post("kd_dosen");
				if($this->web_app_model->cekKodeDosenMax($simpan["kd_dosen"])==0)
				{
					$this->web_app_model->insertData('tbl_dosen',$simpan);
					$lg['username'] = $this->input->post("kd_dosen");
					$lg['password'] = md5($lg['username']);
					$lg['stts'] = "dosen";
					$this->web_app_model->insertData('tbl_login',$lg);
				
				header('location:'.base_url().'admin/dosen');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				}
				else
				{
					$this->session->set_flashdata("save_dosen","
					<p class='alert alert-block alert-success'>
					Kode Dosen Telah Terpakai</p>");
					header('location:'.base_url().'admin/dosen');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_staff()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$simpan["nip"] = $this->input->post("nip");
			$simpan["nama_staff"] = $this->input->post("nama_staff");
			$simpan["kbb"] = $this->input->post("kbb");
			
			if($st=="edit")
			{
				$kd_dosen = $this->input->post('kd_staff');
				$where = array('kd_staff'=>$kd_staff);
				$this->web_app_model->updateDataMultiField("tbl_staff",$simpan,$where);
				
				header('location:'.base_url().'admin/staff');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$simpan["kd_staff"] = $this->input->post("kd_staff");
				if($this->web_app_model->cekKodestaffMax($simpan["kd_staff"])==0)
				{
					$this->web_app_model->insertData('tbl_staff',$simpan);
					$lg['username'] = $this->input->post("kd_staff");
					$lg['password'] = md5($lg['username']);
					$lg['stts'] = "keuangan";
					$this->web_app_model->insertData('tbl_login',$lg);
				
					header('location:'.base_url().'admin/staff');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				}
				else
				{
					$this->session->set_flashdata("save_staff","
					<p class='alert alert-block alert-success'>
					Kode staff Telah Terpakai</p>");
				header('location:'.base_url().'admin/staff');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$id = $this->uri->segment(3);
			$hapus = array('kd_dosen' => $id);
			$hapus2 = array('username' => $id);
			$this->web_app_model->deleteData('tbl_dosen',$hapus);
			$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			$this->web_app_model->deleteData('tbl_login',$hapus2);
			header('location:'.base_url().'admin/dosen');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function dosen_mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Dosen - Mata Kuliah - Sistem Informasi Akademik Online";
			$mn['menu'] = "dosen";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['dosen_mk'] = $this->web_app_model->getDosenMk($this->uri->segment(3));
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_dosen_mk',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Mata Kuliah - Sistem Informasi Akademik Online";
			$mn['menu'] = "mata_kuliah";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['mk'] = $this->web_app_model->getAllData('tbl_mk');
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_mk',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tambah_mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$this->load->view('admin/bg_tambah_mk');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['mk'] = $this->web_app_model->getSelectedData('tbl_mk','kd_mk',$this->uri->segment(3));
			$this->load->view('admin/bg_edit_mk',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$simpan["nama_mk"] = $this->input->post("nama_mk");
			$simpan["jum_sks"] = $this->input->post("jum_sks");
			$simpan["semester"] = $this->input->post("semester");
			$simpan["prasyarat_mk"] = $this->input->post("prasyarat_mk");
			$simpan["kode_jur"] = $this->input->post("kode_jur");
			
			if($st=="edit")
			{
				$kd_mk = $this->input->post('kd_mk');
				$where = array('kd_mk'=>$kd_mk);
				$this->web_app_model->updateDataMultiField("tbl_mk",$simpan,$where);
				
				header('location:'.base_url().'admin/mk');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$simpan["kd_mk"] = $this->input->post("kd_mk");
				if($this->web_app_model->cekKodeMkMax($simpan["kd_mk"])==0)
				{
					$this->web_app_model->insertData('tbl_mk',$simpan);
				
					header('location:'.base_url().'admin/mk');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				}
				else
				{
					$this->session->set_flashdata("save_mk","
					<p class='alert alert-block alert-success'>
					Kode Mata Kuliah Telah Terpakai</p>");
				header('location:'.base_url().'admin/mk');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_mk()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$id = $this->uri->segment(3);
			$hapus = array('kd_mk' => $id);
			$this->web_app_model->deleteData('tbl_mk',$hapus);
			$this->web_app_model->deleteData('tbl_jadwal',$hapus);
			header('location:'.base_url().'admin/mk');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function mk_dosen()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Mata Kuliah - Dosen - Sistem Informasi Akademik Online";
			$mn['menu'] = "mata_kuliah";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['mk_dosen'] = $this->web_app_model->getMkDosen($this->uri->segment(3));
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_mk_dosen',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function mahasiswa()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Mahasiswa - Sistem Informasi Akademik Online";
			$mn['menu'] = "mahasiswa";
			$page=$this->uri->segment(3);
			$limit=20;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;	
		
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			
			$tot_hal = $this->web_app_model->getAllData("tbl_mahasiswa");
			$config['base_url'] = base_url() . 'admin/mahasiswa/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$bc["paginator"] =$this->pagination->create_links();
			
			$bc['mahasiswa'] = $this->web_app_model->getAllData('tbl_mahasiswa');
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_mahasiswa',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_mahasiswa()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['dosen'] = $this->web_app_model->getAllData('tbl_dosen');
			$bc['mahasiswa'] = $this->web_app_model->getEditMahasisiwa($this->uri->segment(3));
			$this->load->view('admin/bg_edit_mahasiswa',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tambah_mahasiswa()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['dosen'] = $this->web_app_model->getAllData('tbl_dosen');
			$bc['kelas'] = $this->web_app_model->getAllData('tbl_kelas');
			$this->load->view('admin/bg_tambah_mahasiswa',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_mahasiswa()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$simpan["nama_mahasiswa"] = $this->input->post("nama_mahasiswa");
			$simpan["angkatan"] = $this->input->post("angkatan");
			$simpan["jurusan"] = $this->input->post("jurusan");
			$simpan["kelas_program"] = $this->input->post("kelas_program");
			$simpan["kbb"] = $this->input->post("kelas");
			$simpan2["kd_dosen"] = $this->input->post("kd_dosen");
			
			if($st=="edit")
			{
				$nim = $this->input->post('nim');
				$where = array('nim'=>$nim);
				$this->web_app_model->updateDataMultiField("tbl_mahasiswa",$simpan,$where);
				$this->web_app_model->updateDataMultiField("tbl_dosen_wali",$simpan2,$where);
				
				header('location:'.base_url().'admin/mahasiswa');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$simpan["nim"] = $this->input->post("nim");
				$simpan2["nim"] = $this->input->post("nim");
				$simpan2["kd_dosen"] = $this->input->post("kd_dosen");
				$simpan3["username"] = $this->input->post("nim");
				$simpan3["password"] = md5($this->input->post("nim"));
				$simpan3["stts"] = "mahasiswa";
				if($this->web_app_model->cekNimMax($simpan["nim"])==0)
				{
					$this->web_app_model->insertData('tbl_mahasiswa',$simpan);
					$this->web_app_model->insertData('tbl_dosen_wali',$simpan2);
					$this->web_app_model->insertData('tbl_login',$simpan3);
				
				header('location:'.base_url().'admin/mahasiswa');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				}
				else
				{
					$this->session->set_flashdata("save_mahasiswa","
					<p class='alert alert-block alert-success'>
					NIM Telah Terpakai</p>");
				header('location:'.base_url().'admin/mahasiswa');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_mahasiswa()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$id = $this->uri->segment(3);
			$hapus = array('nim' => $id);
			$hapus2 = array('username' => $id);
			$this->web_app_model->deleteData('tbl_mahasiswa',$hapus);
			$this->web_app_model->deleteData('tbl_login',$hapus2);
			$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			header('location:'.base_url().'admin/mahasiswa');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "info";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$bc['info'] = $this->web_app_model->getAllDataLimited_Order_DESC('tbl_info',$offset,$limit);
			$tot_hal =  $this->web_app_model->getAllData('tbl_info');
			$config['base_url'] = base_url() . 'admin/info/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$bc["paginator"] =$this->pagination->create_links();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_info',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$bc['info'] = $this->web_app_model->getSelectedData('tbl_info','kd_info',$this->uri->segment(3));
			$this->load->view('admin/bg_edit_info',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function tambah_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$this->load->view('admin/bg_tambah_info');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$id = $this->uri->segment(3);
			$hapus = array('kd_info' => $id);
			$this->web_app_model->deleteData('tbl_info',$hapus);
			header('location:'.base_url().'admin/info');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_info()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post("stts");
			
			$simpan["judul"] = $this->input->post("judul");
			$simpan["isi"] = $this->input->post("isi");
			
			if($st=="edit")
			{
				$kd_info = $this->input->post('kd_info');
				$where = array('kd_info'=>$kd_info);
				$this->web_app_model->updateDataMultiField("tbl_info",$simpan,$where);
				header('location:'.base_url().'admin/info');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
			else if($st=="tambah")
			{
				$simpan["waktu_post"] = strtotime(date('Y-m-d H:i:s'));
				$this->web_app_model->insertData('tbl_info',$simpan);
				header('location:'.base_url().'admin/info');
				?>
					<!-- 
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "pengaturan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_akun',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$username = $this->session->userdata('username');
			$pass_lama = $this->input->post('pass_lama');
			$pass_baru = $this->input->post('pass_baru');
			$ulangi_pass = $this->input->post('ulangi_pass');
			
			$data['username'] = $username;
			$data['password'] = md5($pass_lama);
			$cek = $this->web_app_model->getSelectedDataMultiple('tbl_login',$data);
			if($cek->num_rows()>0)
			{
				if($pass_baru==$ulangi_pass)
				{
					$simpan['password'] = md5($pass_baru);
					$where = array('username'=>$username);
					$this->web_app_model->updateDataMultiField("tbl_login",$simpan,$where);
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Berhasil mengubah password</p>");
					header('location:'.base_url().'admin/akun');
				}
				else
				{
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Password Tidak Sama</p>");
					header('location:'.base_url().'admin/akun');
				}
			}
			else
			{
				$this->session->set_flashdata("save_akun","
				<p class='alert alert-block alert-success'>
				Password Lama Salah</p>");
				header('location:'.base_url().'admin/akun');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Input Nilai Mahasiswa - Sistem Informasi Akademik Online";
			$mn['menu'] = "mahasiswa";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$tot_hal =  $this->web_app_model->getAllData('tbl_info');
			$config['base_url'] = base_url() . 'admin/info/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$bc["paginator"] =$this->pagination->create_links();
			$bc['mhs'] = $this->web_app_model->getDaftarMahasiswa($offset,$limit);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_daftar_mahasiswa',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function input_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Input Nilai Mahasiswa - Sistem Informasi Akademik Online";
			$mn['menu'] = "mahasiswa";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$nim = $this->uri->segment(3);
			
			$dt_mhs = $this->web_app_model->getSelectedData("tbl_mahasiswa","nim",$nim);
			foreach($dt_mhs->result() as $dm)
			{
				$bc['nama_mhs'] = $dm->nama_mahasiswa;
				$bc['program'] = $dm->kelas_program;
				$bc['jurusan'] = $dm->jurusan;
				$bc['kelas_program'] = $dm->kelas_program;
			}
			
			$bc['detailfrs'] = $this->web_app_model->getDetailKrsPersetujuan($nim,$bc['kelas_program']);
			$bc['khs'] = $this->web_app_model->getNilai($nim);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_input_nilai',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$nim = $this->uri->segment(3);
			$kd_mk = $this->uri->segment(4);
			$bc['edit'] = $this->web_app_model->getEditDetailNilai($nim,$kd_mk);
			
			$this->load->view('admin/bg_edit_nilai',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function form_input_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$nim = $this->uri->segment(3);
			$kd_jdw = $this->uri->segment(4);
			$cek_smt = $this->web_app_model->getSelectedData('tbl_perwalian_header','nim',$nim);
			$bc['smt'] = "";
			foreach($cek_smt->result() as $c)
			{
				$bc['smt'] = $c->semester;
			}
			$bc['input'] = $this->web_app_model->getInputDetailNilai($nim,$kd_jdw);
			
			$this->load->view('admin/bg_form_input_nilai',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$st = $this->input->post('stts');
			
			if($st=='edit')
			{
				//$di['nim'] = $this->input->post('nim');
				//$di['kd_mk'] = $this->input->post('kd_mk');
				$nim = $this->input->post('nim');
				$kd_mk = $this->input->post('kd_mk');
				$di['kd_dosen'] = $this->input->post('kd_dosen');
				$di['kd_tahun'] = $this->input->post('kd_tahun');
				$di['semester_ditempuh'] = $this->input->post('semester_ditempuh');
				$di['grade'] = $this->input->post('grade');
				$this->web_app_model->updateDataMultiField('tbl_nilai',$di,array('nim'=>$nim, 'kd_mk'=>$kd_mk));
				?>
					<script>
					window.parent.location.reload(true);
					</script>
				<?php
			}
			
			else if($st=='tambah')
			{
				$di['nim'] = $this->input->post('nim');
				$di['kd_mk'] = $this->input->post('kd_mk');
				$di['kd_dosen'] = $this->input->post('kd_dosen');
				$di['kd_tahun'] = $this->input->post('kd_tahun');
				$di['semester_ditempuh'] = $this->input->post('semester_ditempuh');
				$di['grade'] = $this->input->post('grade');
				$this->web_app_model->insertData('tbl_nilai',$di);
				?>
					<script>
					window.parent.location.reload(true);
					</script>
				<?php
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$dl['nim'] = $this->uri->segment(3);
			$dl['kd_mk'] = $this->uri->segment(4);
			$this->web_app_model->deleteData('tbl_nilai',$dl);
			header('location:'.base_url().'admin/input_nilai/'.$dl['nim']);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	public function tahun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
		{
			$d['judul'] = "Daftar Tahun Akademik - Sistem Informasi Akademik Online";
			$mn['menu'] = "Tahun";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('admin/menu', $mn, true);
			$bc['bio'] = $this->load->view('admin/bio', $bc, true);
			$bc['tahun'] = $this->web_app_model->getAllData('tbl_thn_ajaran');
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('admin/bg_tahun',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
		
		
}



/* End of file admin.php */
/* Location: ./application/controllers/admin.php */