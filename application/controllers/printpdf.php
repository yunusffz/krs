<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Printpdf extends CI_Controller {


// PDF Untuk UTS
		function peserta_printUTS()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
				{
						$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
						$d = explode("_",$this->uri->segment(3));
						$peserta = $this->web_app_model->getPeserta($d[0],$d[1]);
						$peserta_array = $this->web_app_model->getPesertaPrint($d[0],$d[1])->row_array(1);
						 
						$jadwal =  explode("/", $peserta_array['jadwal']);
						$peserta_jumlah = $peserta->num_rows();
						$jam = explode('-',$jadwal[1]);
						
						
						$baselink=$_SERVER['PHP_SELF'];
						if ($peserta_jumlah > 21 ) {
									if($peserta_jumlah >= 22)
										{
										if($peserta_jumlah % 2 == 0){
											$rowsPerPage = $peserta_jumlah / 2 ;
											
											}
											else
											{
												$rowsPerPage = ($peserta_jumlah+1) / 2;
												
											}
										}
									else
											{
												$rowsPerPage = $peserta_jumlah;
												
											}	
							}	
							else
							{
								$rowsPerPage = $peserta_jumlah;
								
							}
		
	

						//nilai halaman awal
						$pageNum = 1;
						
						$offset = ($pageNum - 1) * $rowsPerPage;
						
						$sql = "SELECT DISTINCT c.nim,a.nama_mahasiswa
						FROM tbl_mahasiswa a,tbl_jadwal b,tbl_perwalian_detail c
						WHERE b.kd_mk='".$peserta_array['kd_mk']."' AND c.nim=a.nim AND c.kd_jadwal=b.kd_jadwal AND b.kelas='".$peserta_array['kelas']."' 
						AND b.kelas_program='".$peserta_array['kelas_program']."' ORDER BY c.nim asc LIMIT $offset, $rowsPerPage ";
						$query = mysql_query( $sql );
						
						for ($pageNum=$pageNum;$pageNum<=2;$pageNum++)
						{
						
						$pdf= new FPDF('P','mm','letter');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(15);
						$pdf->setRightMargin(5);
						$pdf->SetFont('arial','B', 12);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',22,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113A Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						$pdf->Cell(0, 10,'ABSENSI UJIAN TENGAH SEMESTER',0,1,'C');
						
						$pdf->Cell(0, 3,strtoupper($bc['tahun_ajaran'] ),0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 11);
						
						$pdf->Cell(120,0,'Kode Matakuliah       : '.$peserta_array['kd_mk'],0,0,'L');
						$pdf->Cell(0,0,'Dosen : '.$peserta_array['nama_dosen'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(120,0,'Nama  Matakuliah     : '.$peserta_array['nama_mk'],0,0,'L');
						$pdf->Cell(0,0,'Kelas / Program : '.$peserta_array['kelas_program'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(120,0,'SKS     : '.$peserta_array['jum_sks'],0,0,'L');
						$pdf->Cell(0,0,'Hari / Waktu / Tempat : '.$peserta_array['uts'],0,1,'R');
						$pdf->Ln(5);
						
						//ISI
						$pdf->SetFont('arial','B', 10);
						
						//Fields Name position
						$Y_Fields_Name_position = 56;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetY($Y_Fields_Name_position);
						$pdf->SetX(21);
						$pdf->Cell(10,5,'No',1,0,'C');
						$pdf->SetX(31);
						$pdf->Cell(20,5,'NIM',1,0,'C');
						$pdf->SetX(51);
						$pdf->Cell(70,5,'Nama Mahasiswa',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(50,5,'Tanda Tangan',1,0,'C');
						$pdf->SetX(171);
						$pdf->Cell(34,5,'Nilai',1,0,'C');
						$pdf->Ln(5);
						
						
						$no=0;
						while($data=mysql_fetch_array($query))
						{
							 $no++;
							 $nim= $data['nim'];
							 $nama = $data['nama_mahasiswa'];
							 
							 //Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',10);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(21);
							$pdf->Cell(10,7,$no,1,0,'C');
							$pdf->SetX(31);
							$pdf->Cell(20,7,$nim,1,0,'C');
							$pdf->SetX(51);
							$pdf->Cell(70,7,strtoupper($nama),1,0,'L');
							 $pdf->SetX(121);
							$pdf->Cell(50,7,' ',1,0,'C');
							$pdf->SetX(171);
							$pdf->Cell(34,7,'  ',1,0,'C');
							$pdf->Ln(7);	
								
						} 
				
						
						$pdf->Ln(3);
						$pdf->Cell(40,2.5,'Jumlah Peserta : '.$no.' Orang',0,1,'L');
						
						//footerr
						$pdf->Ln(15);
						$pdf->SetX(141);
						$pdf->Cell(30,5,' Bandung, ____ / __________/ 20__ ',0,1,'L');
						
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' Pengawas',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,' Dosen',0,1,'C');
						
						$pdf->Ln(15);
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' ( ____________________ )',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,$peserta_array['nama_dosen'],0,1,'C');
						
						
						// ==============================================
					
						$pdf->output();
					
						}
				}
		}
		
				function peserta_printUTS2()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
				{
						$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
						$d = explode("_",$this->uri->segment(3));
						$peserta = $this->web_app_model->getPeserta($d[0],$d[1]);
						$peserta_array = $this->web_app_model->getPesertaPrint($d[0],$d[1])->row_array(1);
						 
						$jadwal =  explode("/", $peserta_array['jadwal']);
						$peserta_jumlah = $peserta->num_rows();
						$jam = explode('-',$jadwal[1]);
						
						
						$baselink=$_SERVER['PHP_SELF'];
						if ($peserta_jumlah > 21 ) {
									if($peserta_jumlah >= 22)
										{
										if($peserta_jumlah % 2 == 0){
											$rowsPerPage = $peserta_jumlah / 2 ;
											
											}
											else
											{
												$rowsPerPage = ($peserta_jumlah+1) / 2;
												
											}
										}
									else
											{
												$rowsPerPage = $peserta_jumlah;
												
											}	
							}	
							else
							{
								$rowsPerPage = $peserta_jumlah;
								
							}
							
						//nilai halaman awal
						$pageNum = 2;
						
						$offset = ($pageNum - 1) * $rowsPerPage;
						
						$sql = "SELECT DISTINCT c.nim,a.nama_mahasiswa
						FROM tbl_mahasiswa a,tbl_jadwal b,tbl_perwalian_detail c
						WHERE b.kd_mk='".$peserta_array['kd_mk']."' AND c.nim=a.nim AND c.kd_jadwal=b.kd_jadwal AND b.kelas='".$peserta_array['kelas']."' 
						AND b.kelas_program='".$peserta_array['kelas_program']."' ORDER BY c.nim asc LIMIT $offset, $rowsPerPage ";
						$query = mysql_query( $sql );
						
						for ($pageNum=$pageNum;$pageNum<=2;$pageNum++)
						{
						
						$pdf= new FPDF('P','mm','letter');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(15);
						$pdf->setRightMargin(5);
						$pdf->SetFont('arial','B', 12);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',22,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113A Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						$pdf->Cell(0, 10,'ABSENSI UJIAN TENGAH SEMESTER',0,1,'C');
						
						$pdf->Cell(0, 3,strtoupper($bc['tahun_ajaran'] ),0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 11);
						
						$pdf->Cell(120,0,'Kode Matakuliah       : '.$peserta_array['kd_mk'],0,0,'L');
						$pdf->Cell(0,0,'Dosen : '.$peserta_array['nama_dosen'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(120,0,'Nama  Matakuliah     : '.$peserta_array['nama_mk'],0,0,'L');
						$pdf->Cell(0,0,'Kelas / Program : '.$peserta_array['kelas_program'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(120,0,'SKS     : '.$peserta_array['jum_sks'],0,0,'L');
						$pdf->Cell(0,0,'Hari / Waktu / Tempat : '.$peserta_array['uts'],0,1,'R');
						$pdf->Ln(5);
						
						//ISI
						$pdf->SetFont('arial','B', 10);
						
						//Fields Name position
						$Y_Fields_Name_position = 56;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetX(21);
						$pdf->Cell(10,5,'No',1,0,'C');
						$pdf->SetX(31);
						$pdf->Cell(20,5,'NIM',1,0,'C');
						$pdf->SetX(51);
						$pdf->Cell(70,5,'Nama Mahasiswa',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(50,5,'Tanda Tangan',1,0,'C');
						$pdf->SetX(171);
						$pdf->Cell(34,5,'Nilai',1,0,'C');
						$pdf->Ln(5);
						
						
						$no=0;
						while($data=mysql_fetch_array($query))
						{
							 $no++;
							 $nim= $data['nim'];
							 $nama = $data['nama_mahasiswa'];
							 
							 //Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',10);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(21);
							$pdf->Cell(10,7,$no,1,0,'C');
							$pdf->SetX(31);
							$pdf->Cell(20,7,$nim,1,0,'C');
							$pdf->SetX(51);
							$pdf->Cell(70,7,strtoupper($nama),1,0,'L');
							 $pdf->SetX(121);
							$pdf->Cell(50,7,' ',1,0,'C');
							$pdf->SetX(171);
							$pdf->Cell(34,7,'  ',1,0,'C');
							$pdf->Ln(7);	
						} 
				
						
						$pdf->Ln(3);
						$pdf->Cell(40,2.5,'Jumlah Peserta : '.$no.' Orang',0,1,'L');
						
						//footerr
						$pdf->Ln(15);
						$pdf->SetX(141);
						$pdf->Cell(30,5,' Bandung, ____ / __________/ 20__ ',0,1,'L');
						
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' Pengawas',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,' Dosen',0,1,'C');
						
						$pdf->Ln(15);
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' ( ____________________ )',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,$peserta_array['nama_dosen'],0,1,'C');
						
						
						// ==============================================
					
						$pdf->output();
					
						}
				}
		}
		
		
// PDF Untuk UAS
		function peserta_printUAS()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
				{
						$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
						$d = explode("_",$this->uri->segment(3));
						$peserta = $this->web_app_model->getPeserta($d[0],$d[1]);
						$peserta_array = $this->web_app_model->getPesertaPrint($d[0],$d[1])->row_array(1);
						 
						$jadwal =  explode("/", $peserta_array['jadwal']);
						$peserta_jumlah = $peserta->num_rows();
						$jam = explode('-',$jadwal[1]);
						
						
						$baselink=$_SERVER['PHP_SELF'];
						if ($peserta_jumlah > 21 ) {
									if($peserta_jumlah >= 22)
										{
										if($peserta_jumlah % 2 == 0){
											$rowsPerPage = $peserta_jumlah / 2 ;
											
											}
											else
											{
												$rowsPerPage = ($peserta_jumlah+1) / 2;
												
											}
										}
									else
											{
												$rowsPerPage = $peserta_jumlah;
												
											}	
							}	
							else
							{
								$rowsPerPage = $peserta_jumlah;
								
							}
		
	

						//nilai halaman awal
						$pageNum = 1;
						
						$offset = ($pageNum - 1) * $rowsPerPage;
						
						$sql = "SELECT DISTINCT c.nim,a.nama_mahasiswa
						FROM tbl_mahasiswa a,tbl_jadwal b,tbl_perwalian_detail c
						WHERE b.kd_mk='".$peserta_array['kd_mk']."' AND c.nim=a.nim AND c.kd_jadwal=b.kd_jadwal AND b.kelas='".$peserta_array['kelas']."' 
						AND b.kelas_program='".$peserta_array['kelas_program']."' ORDER BY c.nim asc LIMIT $offset, $rowsPerPage ";
						$query = mysql_query( $sql );
						
						for ($pageNum=$pageNum;$pageNum<=2;$pageNum++)
						{
						
						$pdf= new FPDF('P','mm','letter');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(10);
						$pdf->SetFont('arial','B', 12);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',15,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113A Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						$pdf->Cell(0, 10,'ABSENSI UJIAN AKHIR SEMESTER',0,1,'C');
						
						$pdf->Cell(0, 3,strtoupper($bc['tahun_ajaran'] ),0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 11);
						
						$pdf->Cell(100,0,'Kode Matakuliah       : '.$peserta_array['kd_mk'],0,0,'L');
						$pdf->Cell(0,0,'Dosen : '.$peserta_array['nama_dosen'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'Nama  Matakuliah     : '.$peserta_array['nama_mk'],0,0,'L');
						$pdf->Cell(0,0,'Kelas / Program : '.$peserta_array['kelas_program'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'SKS     : '.$peserta_array['jum_sks'],0,0,'L');
						$pdf->Cell(0,0,'Ruang : '.$peserta_array['uas'],0,1,'R');
						$pdf->Ln(5);
						
						//ISI
						$pdf->SetFont('arial','B', 10);
						
						//Fields Name position
						$Y_Fields_Name_position = 56;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetY($Y_Fields_Name_position);
						$pdf->SetX(11);
						$pdf->Cell(110,5,'',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(58,5,'Komponen Nilai',1,0,'C');
						$pdf->SetX(179);
						$pdf->Cell(26,5,'Nilai Akhir',1,1,'C');
						$pdf->SetX(11);
						$pdf->Cell(10,8,'No',1,0,'C');
						$pdf->SetX(21);
						$pdf->Cell(20,8,'NIM',1,0,'C');
						$pdf->SetX(41);
						$pdf->Cell(55,8,'Nama Mahasiswa',1,0,'C');
						$pdf->SetX(96);
						$pdf->Cell(25,8,'Tanda Tangan',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(10,8,'UTS',1,0,'C');
						$pdf->Cell(10,8,'UAS',1,0,'C');
						$pdf->Cell(13,8,'TUGAS',1,0,'C');
						$pdf->Cell(12,8,'QUIZ',1,0,'C');
						$pdf->Cell(13,8,'KHDRN',1,0,'C');
						$pdf->Cell(13,8,'NILAI',1,0,'C');
						$pdf->Cell(13,8,'GRADE',1,0,'C');
						$pdf->Ln(8);
						
						
						$no=0;
						while($data=mysql_fetch_array($query))
						{
							 $no++;
							 $nim= $data['nim'];
							 $nama = $data['nama_mahasiswa'];
							 
							 //Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',10);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(11);
							$pdf->Cell(10,7,$no,1,0,'C');
							$pdf->SetX(21);
							$pdf->Cell(20,7,$nim,1,0,'C');
							$pdf->SetX(41);
							$pdf->Cell(55,7,strtoupper($nama),1,0,'L');
							 $pdf->SetX(96);
							$pdf->Cell(25,7,' ',1,0,'C');
							$pdf->SetX(121);
							$pdf->Cell(10,7,'  ',1,0,'C');
							$pdf->Cell(10,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(12,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Ln(7);
						}
						$pdf->Ln(3);
						$pdf->Cell(40,2.5,'Jumlah Peserta : '.$no.' Orang',0,1,'L');
						
						//footerr
						$pdf->Ln(10);
						$pdf->SetX(141);
						$pdf->Cell(30,5,' Bandung, ____ / __________/ 20__ ',0,1,'L');
						
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' Pengawas',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,' Dosen',0,1,'C');
						
						$pdf->Ln(15);
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' ( ____________________ )',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,$peserta_array['nama_dosen'],0,1,'C');
						
						
						// ==============================================
						
						$pdf->output();
					
						}
				}
		}
		
				function peserta_printUAS2()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='admin')
				{
						$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
						$d = explode("_",$this->uri->segment(3));
						$peserta = $this->web_app_model->getPeserta($d[0],$d[1]);
						$peserta_array = $this->web_app_model->getPesertaPrint($d[0],$d[1])->row_array(1);
						 
						$jadwal =  explode("/", $peserta_array['jadwal']);
						$peserta_jumlah = $peserta->num_rows();
						$jam = explode('-',$jadwal[1]);
						
						
						$baselink=$_SERVER['PHP_SELF'];
						if ($peserta_jumlah > 21 ) {
									if($peserta_jumlah >= 22)
										{
										if($peserta_jumlah % 2 == 0){
											$rowsPerPage = $peserta_jumlah / 2 ;
											
											}
											else
											{
												$rowsPerPage = ($peserta_jumlah+1) / 2;
												
											}
										}
									else
											{
												$rowsPerPage = $peserta_jumlah;
												
											}	
							}	
							else
							{
								$rowsPerPage = $peserta_jumlah;
								
							}
							
						//nilai halaman awal
						$pageNum = 2;
						
						$offset = ($pageNum - 1) * $rowsPerPage;
						
						$sql = "SELECT DISTINCT c.nim,a.nama_mahasiswa
						FROM tbl_mahasiswa a,tbl_jadwal b,tbl_perwalian_detail c
						WHERE b.kd_mk='".$peserta_array['kd_mk']."' AND c.nim=a.nim AND c.kd_jadwal=b.kd_jadwal AND b.kelas='".$peserta_array['kelas']."' 
						AND b.kelas_program='".$peserta_array['kelas_program']."' ORDER BY c.nim asc LIMIT $offset, $rowsPerPage ";
						$query = mysql_query( $sql );
						
						for ($pageNum=$pageNum;$pageNum<=2;$pageNum++)
						{
						
						$pdf= new FPDF('P','mm','letter');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(10);
						$pdf->SetFont('arial','B', 10);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',22,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113A Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						$pdf->Cell(0, 10,'ABSENSI UJIAN AKHIR SEMESTER',0,1,'C');
						
						$pdf->Cell(0, 3,strtoupper($bc['tahun_ajaran'] ),0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 11);
						
						$pdf->Cell(100,0,'Kode Matakuliah       : '.$peserta_array['kd_mk'],0,0,'L');
						$pdf->Cell(0,0,'Dosen : '.$peserta_array['nama_dosen'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'Nama  Matakuliah     : '.$peserta_array['nama_mk'],0,0,'L');
						$pdf->Cell(0,0,'Kelas / Program : '.$peserta_array['kelas_program'],0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'SKS     : '.$peserta_array['jum_sks'],0,0,'L');
						$pdf->Cell(0,0,'Ruang : '.$peserta_array['uas'],0,1,'R');
						$pdf->Ln(5);
						
						//ISI
						$pdf->SetFont('arial','B', 10);
						
						//Fields Name position
						$Y_Fields_Name_position = 56;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetY($Y_Fields_Name_position);
						$pdf->SetX(11);
						$pdf->Cell(110,5,'',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(58,5,'Komponen Nilai',1,0,'C');
						$pdf->SetX(179);
						$pdf->Cell(26,5,'Nilai Akhir',1,1,'C');
						$pdf->SetX(11);
						$pdf->Cell(10,8,'No',1,0,'C');
						$pdf->SetX(21);
						$pdf->Cell(20,8,'NIM',1,0,'C');
						$pdf->SetX(41);
						$pdf->Cell(55,8,'Nama Mahasiswa',1,0,'C');
						$pdf->SetX(96);
						$pdf->Cell(25,8,'Tanda Tangan',1,0,'C');
						$pdf->SetX(121);
						$pdf->Cell(10,8,'UTS',1,0,'C');
						$pdf->Cell(10,8,'UAS',1,0,'C');
						$pdf->Cell(13,8,'TUGAS',1,0,'C');
						$pdf->Cell(12,8,'QUIZ',1,0,'C');
						$pdf->Cell(13,8,'KHDRN',1,0,'C');
						$pdf->Cell(13,8,'NILAI',1,0,'C');
						$pdf->Cell(13,8,'GRADE',1,0,'C');
						$pdf->Ln(8);
						
						
						$no=0;
						while($data=mysql_fetch_array($query))
						{
							 $no++;
							 $nim= $data['nim'];
							 $nama = $data['nama_mahasiswa'];
							 
							 //Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',10);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(11);
							$pdf->Cell(10,7,$no,1,0,'C');
							$pdf->SetX(21);
							$pdf->Cell(20,7,$nim,1,0,'C');
							$pdf->SetX(41);
							$pdf->Cell(55,7,strtoupper($nama),1,0,'L');
							 $pdf->SetX(96);
							$pdf->Cell(25,7,' ',1,0,'C');
							$pdf->SetX(121);
							$pdf->Cell(10,7,'  ',1,0,'C');
							$pdf->Cell(10,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(12,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Cell(13,7,'  ',1,0,'C');
							$pdf->Ln(7);
						}
						$pdf->Ln(3);
						$pdf->Cell(40,2.5,'Jumlah Peserta : '.$no.' Orang',0,1,'L');
						
						//footerr
						$pdf->Ln(10);
						$pdf->SetX(141);
						$pdf->Cell(30,5,' Bandung, ____ / __________/ 20__ ',0,1,'L');
						
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' Pengawas',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,' Dosen',0,1,'C');
						
						$pdf->Ln(15);
						$pdf->SetX(11);
						$pdf->Cell(30,2.5,' ( ____________________ )',0,0,'C');
						$pdf->SetX(161);
						$pdf->Cell(30,2.5,$peserta_array['nama_dosen'],0,1,'C');
						
						
						// ==============================================
						
						$pdf->output();
					
						}
				}
		}		
		
		
}