<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen extends CI_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 * @keterangan : Controller untuk halaman khusus dosen
	 */
	 
	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Beranda - Sistem Informasi Akademik Online"; 
			$mn['menu'] = "beranda";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_home',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function persetujuan()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Persetujuan KRS - Sistem Informasi Akademik Online";
			$mn['menu'] = "persetujuan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getMahasiswaBimbingan($bc['kd_dosen']);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_persetujuan',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function detail_krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$bc['nim'] = $this->uri->segment(3);
			$bc['status'] = $this->uri->segment(4);
			$bc['smt_skr'] = $this->web_app_model->getSemesterMax($bc['nim']);
			$bc['ipk'] = $this->web_app_model->getIpk($bc['nim'],$bc['smt_skr']-1);
			$bc['dosen_wali'] = $this->web_app_model->getDosenWali($bc['nim']);
			$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
			$bc['beban_studi'] = beban_studi($bc['ipk']);
			$dt_mhs = $this->web_app_model->getSelectedData("tbl_mahasiswa","nim",$bc['nim']);
			foreach($dt_mhs->result() as $dm)
			{
				$bc['nama_mhs'] = $dm->nama_mahasiswa;
				$bc['program'] = $dm->kelas_program;
				$bc['jurusan'] = $dm->jurusan;
				$bc['kelas_program'] = $dm->kelas_program;
			}
			
			$bc['detailfrs'] = $this->web_app_model->getDetailKrsPersetujuan($bc['nim'],$bc['kelas_program']);
			
			$this->load->view('dosen/bg_detail_krs',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$dt_mentah = $this->input->post('id');
			$dt = explode("|",$dt_mentah);
			$data['nim'] = $dt[0];
			$data['kd_jadwal'] = $dt[1];
			$this->web_app_model->deleteData("tbl_perwalian_detail",$data);
		}
		else
		{
			header('location:'.base_url().'dosen/detail_krs');
		}
	}
	
	function setuju_krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$data_update['nim'] = $this->input->post('nim');
			$data_update['status'] = '1';
			$data_update['tgl_persetujuan'] = date("Y-m-d"); 
			$this->web_app_model->updateData('tbl_perwalian_header',$data_update,$data_update['nim'],'nim');
			//echo"<script>alert('sukses');</script>";
			echo "<div style='width:95%; position:absolute; text-align:center; color:#fff; padding:10px; background-color:red;'>
			Kartu Rencana Studi berhasil disetujui...!!!
			</div>";
			//echo"<script>alert('sukses');</script>";
			 header('location:'.base_url().'dosen/persetujuan');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	function batal_krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$data_update['nim'] = $this->input->post('nim');
			$data_update['status'] = '0';
			$data_update['tgl_persetujuan'] = ""; 
			$this->web_app_model->updateData('tbl_perwalian_header',$data_update,$data_update['nim'],'nim');
			//echo"<script>alert('sukses');</script>";
			echo "<div style='width:95%; position:absolute; text-align:center; color:#fff; padding:10px; background-color:red;'>
			Kartu Rencana Studi berhasil dibatalkan...!!!
			</div>";
			//echo"<script>alert('sukses');</script>";
			 header('location:'.base_url().'dosen/persetujuan');
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Input Nilai Mahasiswa - Sistem Informasi Akademik Online";
			$mn['menu'] = "nilai";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getDaftarMahasiswaNilai($bc['kd_dosen']);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_daftar_mahasiswa',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function input_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Input Nilai Mahasiswa - Sistem Informasi Akademik Online";
			$mn['menu'] = "nilai";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$nim = $this->uri->segment(3);
			
			$dt_mhs = $this->web_app_model->getSelectedData("tbl_mahasiswa","nim",$nim);
			foreach($dt_mhs->result() as $dm)
			{
				$bc['nama_mhs'] = $dm->nama_mahasiswa;
				$bc['program'] = $dm->kelas_program;
				$bc['jurusan'] = $dm->jurusan;
				$bc['kelas_program'] = $dm->kelas_program;
			}
			
			$bc['detailfrs'] = $this->web_app_model->getDetailKrsPersetujuan($nim,$bc['kelas_program']);
			$bc['khs'] = $this->web_app_model->getNilai($nim);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_input_nilai',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function edit_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$nim = $this->uri->segment(3);
			$kd_mk = $this->uri->segment(4);
			$bc['edit'] = $this->web_app_model->getEditDetailNilai($nim,$kd_mk);
			
			$this->load->view('dosen/bg_edit_nilai',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function form_input_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$nim = $this->uri->segment(3);
			$kd_jdw = $this->uri->segment(4);
			$cek_smt = $this->web_app_model->getSelectedData('tbl_perwalian_header','nim',$nim);
			$bc['smt'] = "";
			foreach($cek_smt->result() as $c)
			{
				$bc['smt'] = $c->semester;
			}
			$bc['input'] = $this->web_app_model->getInputDetailNilai($nim,$kd_jdw);
			
			$this->load->view('dosen/bg_form_input_nilai',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$st = $this->input->post('stts');
			
			if($st=='edit')
			{
				//$di['nim'] = $this->input->post('nim');
				//$di['kd_mk'] = $this->input->post('kd_mk');
				$nim = $this->input->post('nim');
				$kd_mk = $this->input->post('kd_mk');
				$di['kd_dosen'] = $this->input->post('kd_dosen');
				$di['kd_tahun'] = $this->input->post('kd_tahun');
				$di['semester_ditempuh'] = $this->input->post('semester_ditempuh');
				$di['grade'] = $this->input->post('grade');
				$this->web_app_model->updateDataMultiField('tbl_nilai',$di,array('nim'=>$nim, 'kd_mk'=>$kd_mk));
				?>
					<!--
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				
				header('location:'.base_url().'dosen/input_nilai/'.$nim );
			}
			
			else if($st=='tambah')
			{
				$di['nim'] = $this->input->post('nim');
				$di['kd_mk'] = $this->input->post('kd_mk');
				$di['kd_dosen'] = $this->input->post('kd_dosen');
				$di['kd_tahun'] = $this->input->post('kd_tahun');
				$di['semester_ditempuh'] = $this->input->post('semester_ditempuh');
				$di['grade'] = $this->input->post('grade');
				$this->web_app_model->insertData('tbl_nilai',$di);
				?>
					<!--
					<script>
					window.parent.location.reload(true);
					</script>
					-->
				<?php
				header('location:'.base_url().'dosen/input_nilai/'.$di['nim'] );
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function hapus_nilai()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$dl['nim'] = $this->uri->segment(3);
			$dl['kd_mk'] = $this->uri->segment(4);
			$this->web_app_model->deleteData('tbl_nilai',$dl);
			header('location:'.base_url().'dosen/input_nilai/'.$dl['nim']);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "pengaturan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_akun',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}

	public function matakuliah()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "matakuliah";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$bc['jadwal'] = $this->web_app_model->getSelectedData("tbl_jadwal","kd_dosen",$bc['kd_dosen']);
			$bc['thn_ajaran'] = $this->web_app_model->getSelectedData("tbl_thn_ajaran","stts","1");	
			$bc['matakuliah'] = $this->web_app_model->getAllData("tbl_mk");	 

			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_matakuliah',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}

	public function detail_absen(){
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "absensi";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$bc['kelas_program'] = $this->uri->segment(3); 
			$bc['kd_mk'] = $this->uri->segment(4);
			$bc['mk'] =  $this->web_app_model->getSelectedData("tbl_mk",
											"kd_mk",$bc['kd_mk']);
			
			$where = array('kd_mk'=>$bc['kd_mk'],'kd_dosen'=>$bc['kd_dosen'],
							'kelas_program'=>$bc['kelas_program']);

			$bc['jadwal'] = $this->web_app_model->getSelectedDataMultiple(
											"tbl_jadwal",$where);

			foreach ($bc['jadwal']->result_array() as $k) {
			//	echo $k['kd_jadwal'];
				
				$bc['absensi'] = $this->web_app_model->getSelectedData("tbl_absensi","kd_jadwal",$k['kd_jadwal']);
				$bc['absen_dosen'] = $this->web_app_model->getSelectedData2("tbl_absensi","kd_jadwal",$k['kd_jadwal'],"Nim",$bc['kd_dosen']);
				
				$bc['mahasiswa'] = $this->web_app_model->getPeserta($k['kd_jadwal'],1);
			}
			

			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_absensi',$bc);
			$this->load->view('global/bg_footer',$d);

//*/
		}
	}
	
	public function simpan_akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$username = $this->session->userdata('kd_dosen');
			$pass_lama = $this->input->post('pass_lama');
			$pass_baru = $this->input->post('pass_baru');
			$ulangi_pass = $this->input->post('ulangi_pass');
			
			$data['username'] = $username;
			$data['password'] = md5($pass_lama);
			$cek = $this->web_app_model->getSelectedDataMultiple('tbl_login',$data);
			if($cek->num_rows()>0)
			{
				if($pass_baru==$ulangi_pass)
				{
					$simpan['password'] = md5($pass_baru);
					$where = array('username'=>$username);
					$this->web_app_model->updateDataMultiField("tbl_login",$simpan,$where);
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Berhasil mengubah password</p>");
					header('location:'.base_url().'dosen/akun');
				}
				else
				{
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Password Tidak Sama</p>");
					header('location:'.base_url().'dosen/akun');
				}
			}
			else
			{
				$this->session->set_flashdata("save_akun","
				<p class='alert alert-block alert-success'>
				Password Lama Salah</p>");
				header('location:'.base_url().'dosen/akun');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	
	//perubahan KRS
	
	public function prs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='dosen')
		{
			$d['judul'] = "Perubahan KRS - Sistem Informasi Akademik Online";
			$mn['menu'] = "perubahan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['username'] = $this->session->userdata('username');
			$bc['kd_dosen'] = $this->session->userdata('kd_dosen');
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			$bc['mhs'] = $this->web_app_model->getMahasiswaBimbinganKRS($bc['kd_dosen']);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_perubahan',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	
	public function detail_prs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Dosen')
		{
			$d['judul'] = "Perubahan Kartu Rencana Studi - Sistem Informasi Akademik Online";
			$mn['menu'] = "Perubahan krs";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['program'] = $this->session->userdata('program');
			$bc['jurusan'] = $this->session->userdata('jurusan');
			$bc['username'] = $this->session->userdata('username');
			$bc['smt_skr'] = $this->web_app_model->getSemesterMax($bc['nim']);
			$bc['ipk'] = $this->web_app_model->getIpk($bc['nim'],$bc['smt_skr']-1);
			$bc['dosen_wali'] = $this->web_app_model->getDosenWali($bc['nim']);
			$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
			$bc['detail_krs'] = $this->web_app_model->getDetailKrs($bc['nim']);
			$bc['beban_studi'] = beban_studi($bc['ipk']);
			$bc['menu'] = $this->load->view('dosen/menu', $mn, true);
			$bc['bio'] = $this->load->view('dosen/bio', $bc, true);
			
			$kls = $this->session->userdata('program');
			$bc['jadwal'] = $this->web_app_model->getJadwal($bc['nim'],$kls);
			$bc['detailfrs'] = $this->web_app_model->getDetailKrsPersetujuan($bc['nim'],$bc['program']);
			
			$st = '';
			$cek = $this->web_app_model->getSelectedData('tbl_perwalian_header','nim',$bc['nim']);
			foreach($cek->result() as $c)
			{
				$st = $c->status;
			}
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('dosen/bg_jadwal',$d);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
}



/* End of file dosen.php */
/* Location: ./application/controllers/dosen.php */