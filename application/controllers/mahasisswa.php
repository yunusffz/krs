<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 * @keterangan : Controller untuk halaman khusus mahasiswa
	 */
	
	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Beranda - Sistem Informasi Akademik Online";
			$mn['menu'] = "beranda";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			
			//
			
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('mahasiswa/bg_home',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Kartu Rencana Studi - Sistem Informasi Akademik Online";
			$mn['menu'] = "krs";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['program'] = $this->session->userdata('program');
			//$bc['kelas'] = $this->session->userdata('kelas');
			$bc['jurusan'] = $this->session->userdata('jurusan');
			$bc['username'] = $this->session->userdata('username');
			$bc['smt_skr'] = $this->web_app_model->getSemesterMax($bc['nim']);
			$bc['ipk'] = $this->web_app_model->getIpk($bc['nim'],$bc['smt_skr']-1);
			$bc['dosen_wali'] = $this->web_app_model->getDosenWali($bc['nim']);
			$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
			$bc['detail_krs'] = $this->web_app_model->getDetailKrs($bc['nim']);
			$bc['beban_studi'] = beban_studi($bc['ipk']);
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			
			$kls = $this->session->userdata('program');
			$grp = $this->session->userdata('kbb');
			$bc['jadwal'] = $this->web_app_model->getJadwal($bc['nim'],$kls,$grp);
			$bc['detailfrs'] = $this->web_app_model->getDetailKrsPersetujuan($bc['nim'],$bc['program']);
			
			$st = '';
			$cek = $this->web_app_model->getSelectedData('tbl_perwalian_header','nim',$bc['nim']);
			foreach($cek->result() as $c)
			{
				$st = $c->status;
			}
			
			$this->load->view('global/bg_top',$d);
			
			if($st==0)
			{
				$this->load->view('mahasiswa/bg_jadwal',$bc);
			}
			else if($st==1)
			{
				$this->load->view('mahasiswa/bg_detail_krs',$bc);
			}
			
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function khs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Kartu Hasil Studi - Sistem Informasi Akademik Online";
			$mn['menu'] = "khs";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			$bc['khs'] = $this->web_app_model->getNilai($bc['nim']);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('mahasiswa/bg_nilai',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_krs()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$nim = $this->input->post('nim');
			$smt = $this->input->post('semester');
			$detailfrs = $this->input->post('detailfrs');
			if($detailfrs!="")
			{
				$data_header=array(
					'nim' => $nim ,
					'tgl_perwalian' => date("Y-m-d"),
					'tgl_persetujuan' => "",
					'status' => "0",
					'semester' => $smt);
				$data_detail=array();
				$temp=explode("|", $detailfrs);
				foreach($temp as $value) 
				{
					$data_detail[]=array(
					'nim' => $nim ,
					'kd_jadwal' => $value);
				}
				$this->web_app_model->deleteKrs($nim,$smt);
				$this->web_app_model->insertKrs($data_header,$data_detail);
				$this->session->set_flashdata('save_krs', '<p class="alert alert-block alert-success">Kartu Rencana Studi berhasil disimpan...!!! Silahkan menghubungi Dosen Wali .</p>');
				header('location:'.base_url().'mahasiswa/krs');
			}
			else{
				$this->web_app_model->deleteKrs($nim,$smt);
				header('location:'.base_url().'mahasiswa/krs');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function peserta()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d = explode("_",$this->uri->segment(3));
			$bc['peserta'] = $this->web_app_model->getPeserta($d[0],$d[1]);
			
			$this->load->view('mahasiswa/bg_peserta',$bc);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function info_kampus()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "info";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$bc['info'] = $this->web_app_model->getAllDataLimited_Order_DESC('tbl_info',$offset,$limit);
			$tot_hal =  $this->web_app_model->getAllData('tbl_info');
			$config['base_url'] = base_url() . 'mahasiswa/info_kampus/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$bc["paginator"] =$this->pagination->create_links();
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('mahasiswa/bg_info',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Info Kampus - Sistem Informasi Akademik Online";
			$mn['menu'] = "pengaturan";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('mahasiswa/bg_akun',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function simpan_akun()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$username = $this->session->userdata('nim');
			$pass_lama = $this->input->post('pass_lama');
			$pass_baru = $this->input->post('pass_baru');
			$ulangi_pass = $this->input->post('ulangi_pass');
			
			$data['username'] = $username;
			$data['password'] = md5($pass_lama);
			$cek = $this->web_app_model->getSelectedDataMultiple('tbl_login',$data);
			if($cek->num_rows()>0)
			{
				if($pass_baru==$ulangi_pass)
				{
					$simpan['password'] = md5($pass_baru);
					$where = array('username'=>$username);
					$this->web_app_model->updateDataMultiField("tbl_login",$simpan,$where);
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Berhasil mengubah password</p>");
					header('location:'.base_url().'mahasiswa/akun');
				}
				else
				{
					$this->session->set_flashdata("save_akun","
					<p class='alert alert-block alert-success'>
					Password Tidak Sama</p>");
					header('location:'.base_url().'mahasiswa/akun');
				}
			}
			else
			{
				$this->session->set_flashdata("save_akun","
				<p class='alert alert-block alert-success'>
				Password Lama Salah</p>");
				header('location:'.base_url().'mahasiswa/akun');
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	public function kalendar()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$d['judul'] = "Kalendar Akademik - Sistem Informasi Akademik Online";
			$mn['menu'] = "Kalendar Akademik";
			$bc['nama'] = $this->session->userdata('nama');
			$bc['status'] = $this->session->userdata('stts');
			$bc['nim'] = $this->session->userdata('nim');
			$bc['username'] = $this->session->userdata('username');
			$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
			$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
			
			$this->load->view('global/bg_top',$d);
			$this->load->view('mahasiswa/bg_kalendar',$bc);
			$this->load->view('global/bg_footer',$d);
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}
	
	
	// PDF Untuk UTS
	function cetakUTS()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$bc['nim'] = $this->session->userdata('nim');
			
			//pengecekan pembayaran ujian			
			$bc['kode']=$this->web_app_model->getAktifasiUTS($bc['nim']);
			if ($bc['kode']=='AKTIF')
			{
		
			$bc['nama'] = $this->session->userdata('nama');
			$bc['jurusan'] = $this->session->userdata('jurusan');
			if($bc['jurusan']=='SI')
				{
					$jur='SISTEM INFORMASI';
				}
				else if(($bc['jurusan']=='IF'))
				{
					$jur='TEKNIK INFORMATIKA';
				}
			$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
	
				$sql = "SELECT DISTINCT ( @NO_URUT := @NO_URUT + 1 ) no,g.nim,a.kd_mk,b.nama_mk,a.kd_dosen,a.uts,a.kelas_program
				FROM tbl_perwalian_detail g,tbl_jadwal a,tbl_mk b,(SELECT @NO_URUT := 0 ) VARIABLE_URUT, tbl_thn_ajaran f
					WHERE a.kd_mk=b.kd_mk AND g.kd_jadwal=a.kd_jadwal
					AND g.nim='".$bc['nim']."' AND a.kd_tahun=f.kd_tahun AND f.stts=1";
				$query = mysql_query( $sql );
		
		
		
						$pdf= new FPDF('L','mm','A5');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(10);
						$pdf->SetFont('arial','B', 10);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',22,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113 Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						

						$pdf->Cell(0, 5,'KARTU UJIAN TENGAH SEMESTER',0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 9);
						
						$pdf->Cell(100,0,'NIM         : '.$bc['nim'],0,0,'L');
						$pdf->Cell(0,0,'JURUSAN  : '.$jur,0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'NAMA     : '.strtoupper($bc['nama']),0,0,'L');
						$pdf->Cell(0,0,'SEMESTER       : '.strtoupper($bc['tahun_ajaran']),0,1,'R');
						//ISI
						$pdf->SetFont('arial','B', 8);
						
						//Fields Name position
						$Y_Fields_Name_position = 42;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetY($Y_Fields_Name_position);
						$pdf->SetX(10);
						$pdf->Cell(10,5,'No',1,0,'C');
						$pdf->SetX(20);
						$pdf->Cell(80,5,'NAMA MATAKULIAH',1,0,'C');
						$pdf->SetX(100);
						$pdf->Cell(40,5,'TANGGAL / JAM / RUANG',1,0,'C');
						$pdf->SetX(140);
						$pdf->Cell(20,5,'DOSEN',1,0,'C');
						$pdf->SetX(160);
						$pdf->Cell(40,5,'TTD PENGAWAS',1,0,'C');
						$pdf->Ln(5);
						
						
						while($data=mysql_fetch_array($query))
						{
							 $no = $data['no'];;
							 $namamk = $data['nama_mk'];
							 $tanggal = $data['uts'];
							 $ruangan = $data['kelas_program'];
							 $dosen = $data['kd_dosen'];
							 
							//Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',8);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(10);
							$pdf->Cell(10,5,$no,1,0,'C');
							$pdf->SetX(20);
							$pdf->Cell(80,5,$namamk,1,0,'L');
							$pdf->SetX(100);
							$pdf->Cell(40,5,$tanggal,1,0,'C');
							$pdf->SetX(140);
							$pdf->Cell(20,5,$dosen,1,0,'C');
							$pdf->SetX(160);
							$pdf->Cell(40,5,'',1,0,'C');
						
							$pdf->Ln(5);
							
						
						}
						//FOTO
						$Y_Table_Position = 99;
						$pdf->SetY($Y_Table_Position);
						$pdf->SetX(100);
						$pdf->Cell(22,28,'2x3',1,1,'C');
						
						
						//tandatangankanan
						
						
						$pdf->SetFont('Arial','',8);
						$Y_Table_Position = 104;
						$pdf->SetY($Y_Table_Position);
						$pdf->Cell(190,15,'Bandung , '.date('d-m-Y'),0,1,'R');
						$pdf->SetFont('Arial','U',8);
						$pdf->SetY($Y_Table_Position+=20);
						$pdf->Cell(190,0,'Uro Abdrohim,S.kom,M.T',0,1,'R');
						$pdf->SetFont('Arial','',8);
						$pdf->SetY($Y_Table_Position+=2);
						$pdf->Cell(190,2,'Direktur Operasional   ',0,1,'R');
						
						//footer.A>/Az?A
						$pdf->SetFont('Arial','',8);
						$Y_Table_Position = 104;
						$pdf->SetY($Y_Table_Position);
						$pdf->SetFont('Arial','',6);
						$pdf->SetX(10);
						$pdf->Cell(40,2.5,'Keterangan : ',0,1,'L');
						$pdf->Cell(40,2.5,'1. Kartu Ujian wajib dibawa saat ujian berlangsung. ',0,1,'L');
						$pdf->Cell(40,2.5,'2. Peserta Ujian wajib Hadir minimal 30 menit sebelum Pelaksanaan Ujian.',0,1,'L');
						$pdf->Cell(40,2.5,'3. Peserta Ujian Wajib mentaati TATA TERTIB Yang berlaku.',0,1,'L');
						
						$pdf->Ln();
						
						
						//barcode 
						
						$pdf->Output('KartuUjianUTS'.$bc['nim'].'.pdf','D');
						
						}
					else
						{
							$d['judul'] = "Beranda - Sistem Informasi Akademik Online";
							$mn['menu'] = "beranda";
							$bc['nama'] = $this->session->userdata('nama');
							$bc['status'] = $this->session->userdata('stts');
							$bc['nim'] = $this->session->userdata('nim');
							$bc['username'] = $this->session->userdata('username');
							$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
							$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
							$this->load->view('global/bg_top',$d);
							$this->load->view('mahasiswa/bg_alert',$bc);
							$this->load->view('global/bg_footer',$d);
							}
			}			
		}
		
		
	// PDF Untuk UAS
		function cetakUAS()
		{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mahasiswa')
		{
			$bc['nim'] = $this->session->userdata('nim');
			
			//pengecekan pembayaran ujian			
			$bc['kode']=$this->web_app_model->getAktifasiUAS($bc['nim']);
			if ($bc['kode']=='AKTIF2')
			{
		
			$bc['nama'] = $this->session->userdata('nama');
			$bc['jurusan'] = $this->session->userdata('jurusan');
			if($bc['jurusan']=='SI')
				{
					$jur='SISTEM INFORMASI';
				}
				else if(($bc['jurusan']=='IF'))
				{
					$jur='TEKNIK INFORMATIKA';
				}
			$bc['tahun_ajaran'] = $this->web_app_model->getTahunAjaran();
	
				$sql = "SELECT DISTINCT ( @NO_URUT := @NO_URUT + 1 ) no,g.nim,a.kd_mk,b.nama_mk,a.kd_dosen,a.uas,a.kelas_program
				FROM tbl_perwalian_detail g,tbl_jadwal a,tbl_mk b,(SELECT @NO_URUT := 0 ) VARIABLE_URUT, tbl_thn_ajaran f
					WHERE a.kd_mk=b.kd_mk AND g.kd_jadwal=a.kd_jadwal
					AND g.nim='".$bc['nim']."' AND a.kd_tahun=f.kd_tahun AND f.stts=1";
				$query = mysql_query( $sql );
		
		
		
						$pdf= new FPDF('L','mm','A5');
						$pdf->setTopMargin(2);
						$pdf->setLeftMargin(10);
						$pdf->SetFont('arial','B', 10);

						$pdf->AddPage();
						$pdf->Image('assets\stmik_bandung.png',22,5,35,15);
						
						//JUDUL
						$pdf->Cell(0, 5,'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER BANDUNG',0,1,'R');
						$pdf->Cell(0, 5,'Jl. Cikutra 113 Telp. (022) 7207777. Fax.(022) 7207777',0,1,'R');
						$pdf->Cell(0, 5,'BANDUNG - JAWABARAT - INDONESIA',0,1,'R');
						$pdf->Cell(0, 5,'| e-mail : info@stmik-bandung.ac.id. | Website : www.stmik-bandung.ac.id |',0,1,'R');
						$pdf->Ln(1);
						$pdf->Cell(0, 0.5,' ',1,1,'C');
						//Header
						

						$pdf->Cell(0, 5,'KARTU UJIAN AKHIR SEMESTER',0,1,'C');
						$pdf->Ln(3);
						$pdf->SetFont('arial','', 9);
						
						$pdf->Cell(100,0,'NIM         : '.$bc['nim'],0,0,'L');
						$pdf->Cell(0,0,'JURUSAN  : '.$jur,0,1,'R');
						$pdf->Ln(5);
						$pdf->Cell(100,0,'NAMA     : '.strtoupper($bc['nama']),0,0,'L');
						$pdf->Cell(0,0,'SEMESTER       : '.strtoupper($bc['tahun_ajaran']),0,1,'R');
						//ISI
						$pdf->SetFont('arial','B', 8);
						
						//Fields Name position
						$Y_Fields_Name_position = 42;

						//First create each Field Name
						//Gray color filling each Field Name box
						$pdf->SetFillColor(110,180,230);
						//Bold Font for Field Name
						$pdf->SetY($Y_Fields_Name_position);
						$pdf->SetX(10);
						$pdf->Cell(10,5,'No',1,0,'C');
						$pdf->SetX(20);
						$pdf->Cell(80,5,'NAMA MATAKULIAH',1,0,'C');
						$pdf->SetX(100);
						$pdf->Cell(40,5,'TANGGAL / JAM / RUANG',1,0,'C');
						$pdf->SetX(140);
						$pdf->Cell(20,5,'DOSEN',1,0,'C');
						$pdf->SetX(160);
						$pdf->Cell(40,5,'TTD PENGAWAS',1,0,'C');
						$pdf->Ln(5);
						
						
						while($data=mysql_fetch_array($query))
						{
							 $no = $data['no'];;
							 $namamk = $data['nama_mk'];
							 $tanggal = $data['uas'];
							 $ruangan = $data['kelas_program'];
							 $dosen = $data['kd_dosen'];
							 
							//Table position, under Fields Name
							$Y_Table_Position = 50;

							//Now show the columns
							$pdf->SetFont('Arial','',8);

							//$pdf->SetY($Y_Table_Position);
							$pdf->SetX(10);
							$pdf->Cell(10,5,$no,1,0,'C');
							$pdf->SetX(20);
							$pdf->Cell(80,5,$namamk,1,0,'L');
							$pdf->SetX(100);
							$pdf->Cell(40,5,$tanggal,1,0,'C');
							$pdf->SetX(140);
							$pdf->Cell(20,5,$dosen,1,0,'C');
							$pdf->SetX(160);
							$pdf->Cell(40,5,'',1,0,'C');
						
							$pdf->Ln(5);
							
						
						}
						//FOTO
						$Y_Table_Position = 99;
						$pdf->SetY($Y_Table_Position);
						$pdf->SetX(100);
						$pdf->Cell(22,28,'2x3',1,1,'C');
						
						
						//tandatangankanan
						
						
						$pdf->SetFont('Arial','',8);
						$Y_Table_Position = 104;
						$pdf->SetY($Y_Table_Position);
						$pdf->Cell(190,15,'Bandung , '.date('d-m-Y'),0,1,'R');
						$pdf->SetFont('Arial','U',8);
						$pdf->SetY($Y_Table_Position+=20);
						$pdf->Cell(190,0,'Uro Abdrohim,S.kom,M.T',0,1,'R');
						$pdf->SetFont('Arial','',8);
						$pdf->SetY($Y_Table_Position+=2);
						$pdf->Cell(190,2,'Direktur Operasional   ',0,1,'R');
						
						//footer.A>/Az?A
						$pdf->SetFont('Arial','',8);
						$Y_Table_Position = 104;
						$pdf->SetY($Y_Table_Position);
						$pdf->SetFont('Arial','',6);
						$pdf->SetX(10);
						$pdf->Cell(40,2.5,'Keterangan : ',0,1,'L');
						$pdf->Cell(40,2.5,'1. Kartu Ujian wajib dibawa saat ujian berlangsung. ',0,1,'L');
						$pdf->Cell(40,2.5,'2. Peserta Ujian wajib Hadir minimal 30 menit sebelum Pelaksanaan Ujian.',0,1,'L');
						$pdf->Cell(40,2.5,'3. Peserta Ujian Wajib mentaati TATA TERTIB Yang berlaku.',0,1,'L');
						
						$pdf->Ln();
						
						
						//barcode 
						
						$pdf->Output('KartuUjianUAS'.$bc['nim'].'.pdf','D');
						
						}
					else
						{
							$d['judul'] = "Beranda - Sistem Informasi Akademik Online";
							$mn['menu'] = "beranda";
							$bc['nama'] = $this->session->userdata('nama');
							$bc['status'] = $this->session->userdata('stts');
							$bc['nim'] = $this->session->userdata('nim');
							$bc['username'] = $this->session->userdata('username');
							$bc['menu'] = $this->load->view('mahasiswa/menu', $mn, true);
							$bc['bio'] = $this->load->view('mahasiswa/bio', $bc, true);
							$this->load->view('global/bg_top',$d);
							$this->load->view('mahasiswa/bg_alert',$bc);
							$this->load->view('global/bg_footer',$d);
							}
			}			
		}
}

/* End of file mahasiswa.php */
/* Location: ./application/controllers/mahasiswa.php */